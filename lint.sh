#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}

if [[ $CHECK_ONLY == "true" ]]
then
    BLACK_EXTRA_OPTS="--check --diff"
    ISORT_EXTRA_OPTS="--check-only --diff"
fi


echo "-- Checking for architecture errors"

if grep -qFrin "from ryax.authorization.application." ryax/authorization/infrastructure/
then
    echo -e "\e[31mWARNING\e[0m: Some infrastructure modules import application code:"
    grep -Frin --color "from ryax.authorization.application." ryax/authorization/infrastructure/
fi

if grep -qFrin "from ryax.authorization.application." ryax/authorization/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import application code:"
    grep -Frin --color "from ryax.authorization.application." ryax/authorization/domain
fi

if grep -qFrin "from ryax.authorization.infrastructure." ryax/authorization/domain
then
    echo -e "\e[31mWARNING\e[0m: Some domain modules import infrastructure code:"
    grep -Frin --color "from ryax.authorization.infrastructure." ryax/authorization/domain
fi

# Detect cross-infrastructure includes, except towards utils.
for dir in $(ls -d  ryax/authorization/infrastructure/*/)
do
    dir_name=$(basename $dir)
    if grep -Frin "from ryax.authorization.infrastructure." $dir | grep -Fv "ryax.authorization.infrastructure.utils" | grep -Fvq "ryax.authorization.infrastructure.$dir_name"
    then
        echo -e "\e[31mWARNING\e[0m: There are cross-infrastructure includes in $dir_name:"
        grep -Frin "from ryax.authorization.infrastructure." $dir | grep -Fv "ryax.authorization.infrastructure.utils" | grep -Fv --color ryax.authorization.infrastructure.$dir_name
    fi
done


echo "-- Checking import sorting"
isort -s .venv -s migrations -s ci -s .poetry --filter-files $ISORT_EXTRA_OPTS $TO_CHECK_DIR

echo "-- Checking python formating"
black $TO_CHECK_DIR --exclude "docs|ci|ryaxpkgs|migrations|.*pb2.py|.poetry" $BLACK_EXTRA_OPTS

echo "-- Checking python static checking"
flake8 $TO_CHECK_DIR --exclude="docs/*,ci/*,ryaxpkgs/*,migrations/*,.venv/*,*pb2.py,.poetry/*"

echo "-- Checking type annotations"
mypy ./ryax --exclude /*pb2.py
