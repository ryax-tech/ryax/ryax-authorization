# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from aiohttp import web
from logfmt_logger import getLogger

from .application.project_service import ProjectService
from .application.user_service import UserService
from .container import ApplicationContainer
from .domain.project.project_values import AddUserToProjectData, ProjectCreateData
from .domain.user.user_values import UserCreateData, UserRoles
from .infrastructure.api.setup import setup as api_setup
from .version import __version__

logger = getLogger(__name__)


def _get_log_level(str_level: str) -> int:
    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    return numeric_level


def init() -> web.Application:
    """Init application"""
    container = ApplicationContainer()

    container.configuration.jwt_secret_key.from_env(
        "RYAX_JWT_SECRET_KEY", "secret_key_for_jwt"
    )
    container.configuration.ryax_datastore.from_env("RYAX_DATASTORE", required=True)

    container.configuration.default_project.name.from_env(
        "RYAX_DEFAULT_PROJECT_NAME", "Default Project"
    )
    container.configuration.default_project.id.from_env(
        "RYAX_DEFAULT_PROJECT_ID", "eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2"
    )
    container.configuration.default_user.name.from_env(
        "RYAX_DEFAULT_USER_NAME", "user1"
    )
    container.configuration.default_user.password.from_env(
        "RYAX_DEFAULT_USER_PASS", "pass1"
    )

    container.configuration.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    container.configuration.sqlalchemy_log_level.from_env(
        "RYAX_SQLALCHEMY_LOG_LEVEL", "INFO"
    )

    # Set log level
    str_level = container.configuration.log_level()
    numeric_level = _get_log_level(str_level)
    logging.basicConfig(
        level=numeric_level,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    logger.info("Logging level is set to %s" % str_level.upper())

    str_level = container.configuration.sqlalchemy_log_level()
    numeric_level = _get_log_level(str_level)
    logging.getLogger("sqlalchemy.engine").setLevel(numeric_level)
    logger.info("Logging level of sqlalchemy is set to %s" % str_level.upper())

    app: web.Application = web.Application()
    api_setup(app, container)
    app["container"] = container

    return app


async def initialize_default_user_and_project(container: ApplicationContainer) -> None:
    """
    If no users exist on startup, create the default admin user.
    If no projects exist on startup, create the default project.
    If both of the above were just created, assign user1 to the default project
    """
    user_service: UserService = container.user_service()
    project_service: ProjectService = container.project_service()
    created_default_user_id = ""
    created_default_user_name = ""

    if not user_service.list_users():
        created_default_user_name = container.configuration.default_user.name()
        default_user = UserCreateData(
            username=created_default_user_name,
            password=container.configuration.default_user.password(),
            role=UserRoles.admin,
            email="info@ryax.tech",
            comment="Default ryax user",
        )
        created_default_user_id = user_service.create_user(default_user)
        logger.info(
            f"Created default user: name: {created_default_user_name}, id: {created_default_user_id}"
        )

    if not project_service.list_projects():
        default_project_name = container.configuration.default_project.name()
        default_project_id = container.configuration.default_project.id()
        create_project_data = ProjectCreateData(
            name=default_project_name, id=default_project_id
        )
        project_id = project_service.create_project(create_project_data)
        logger.info(
            f"Created default project: name: {default_project_name}, id: {project_id}"
        )
        if created_default_user_id:
            add_user_data = AddUserToProjectData(user_id=created_default_user_id)
            project_service.add_user_to_project(project_id, add_user_data)
            logger.info(
                f"Assigned user: '{created_default_user_name}' to project: '{default_project_name}'."
            )


async def on_startup(app: web.Application) -> None:
    """Hooks for application startup"""
    container: ApplicationContainer = app["container"]
    container.database_engine().connect()
    await initialize_default_user_and_project(container)


async def on_cleanup(app: web.Application) -> None:
    """Define hook when application stop"""
    container: ApplicationContainer = app["container"]
    container.database_engine().disconnect()


def start() -> None:
    """Start application"""
    print(f"Authorization version: {__version__}")
    app: web.Application = init()
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)
    web.run_app(app)


def db_migration() -> None:
    import alembic.config

    alembic_args = ["upgrade", "head"]
    alembic.config.main(argv=alembic_args)
