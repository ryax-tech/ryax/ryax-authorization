# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.


class UserNotFoundException(Exception):
    message: str = "User not found"


class UserHasNoProjectException(Exception):
    message: str = "User has not current project"


class UserAlreadyExistsException(Exception):
    message: str = "Username already exists"


class NoMoreAdminsIfDoneException(Exception):
    message: str = (
        "Cannot perform this operation, otherwise Ryax will not have any administrators"
    )


class RoleUpdateForbiddenError(Exception):
    message: str = "Only an administrator can update user role"
