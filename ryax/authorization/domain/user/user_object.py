# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional

from ryax.authorization.domain.user.user_values import (
    UserRoles,
    UserStates,
    UserUpdateData,
)

if TYPE_CHECKING:
    from ryax.authorization.domain.project.project_object import Project


@dataclass
class User:
    id: str
    username: str
    password: str
    created_at: datetime.datetime
    modified_at: datetime.datetime
    role: UserRoles
    state: UserStates
    email: str
    comment: str
    current_project: Optional["Project"] = None

    def update_data(
        self, user_data: UserUpdateData, created_password: Optional[str]
    ) -> None:
        self.username = user_data.username if user_data.username else self.username
        self.password = created_password if created_password else self.password
        self.modified_at = datetime.datetime.now(datetime.timezone.utc)
        self.role = user_data.role if user_data.role else self.role
        self.email = user_data.email if user_data.email else self.email
        self.comment = user_data.comment if user_data.comment else self.comment

    def set_current_project(self, project: "Project") -> None:
        self.current_project = project
