# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
import uuid

from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    UserCreateData,
    UserRoles,
    UserStates,
)


class UserFactory:
    def __init__(self) -> None:
        pass

    def from_create_user(
        self, user_data: UserCreateData, created_password: str
    ) -> User:
        return User(
            id=str(uuid.uuid4()),
            username=user_data.username,
            password=created_password,
            created_at=datetime.datetime.now(datetime.timezone.utc),
            modified_at=datetime.datetime.now(datetime.timezone.utc),
            role=UserRoles(user_data.role),
            state=UserStates.ok,
            email=user_data.email,
            comment=user_data.comment,
        )
