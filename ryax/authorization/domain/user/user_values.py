# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import enum
from dataclasses import dataclass
from typing import Optional


class UserRoles(enum.Enum):
    admin = "Admin"
    developer = "Developer"
    user = "User"
    operator = "Operator"
    anonymous = "Anonymous"


class UserStates(enum.Enum):
    ok = "OK"
    creating = "Creating"


@dataclass
class UserCreateData:
    username: str
    password: str
    role: UserRoles
    email: str
    comment: str


@dataclass
class UserUpdateData:
    username: Optional[str] = None
    password: Optional[str] = None
    role: Optional[UserRoles] = None
    email: Optional[str] = None
    comment: Optional[str] = None


@dataclass
class SetCurrentProjectData:
    project_id: str
