# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from ..project.project_object import Project
from .user_object import User


class IUserRepository(abc.ABC):
    @abc.abstractmethod
    def list_users(self) -> List[User]:
        pass

    @abc.abstractmethod
    def add_user(self, user: User) -> None:
        pass

    @abc.abstractmethod
    def get_user(self, user_id: str) -> User:
        pass

    @abc.abstractmethod
    def delete_user(self, user: User) -> None:
        pass

    @abc.abstractmethod
    def get_user_by_username(self, username: str) -> User:
        pass

    @abc.abstractmethod
    def get_user_projects(self, user: User) -> List[Project]:
        pass

    @abc.abstractmethod
    def number_of_admins(self) -> int:
        pass
