# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from enum import Enum
from typing import Optional


class ProjectState(Enum):
    NONE = ("none",)
    READY = "ready"


@dataclass
class ProjectUpdateData:
    name: str


@dataclass
class ProjectCreateData:
    name: str
    id: Optional[str] = None


@dataclass
class AddUserToProjectData:
    user_id: str
