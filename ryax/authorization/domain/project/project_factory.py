# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
import uuid

from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import ProjectCreateData


class ProjectFactory:
    def __init__(self) -> None:
        pass

    def from_create_project(self, project_data: ProjectCreateData) -> Project:
        if project_data.id is not None:
            project_id = project_data.id
        else:
            project_id = str(uuid.uuid4())
        return Project(
            id=project_id,
            name=project_data.name,
            created_at=datetime.datetime.now(datetime.timezone.utc),
        )
