# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from ryax.authorization.domain.project.project_object import Project


class IProjectRepository(abc.ABC):
    @abc.abstractmethod
    def list_projects(self) -> List[Project]:
        pass

    @abc.abstractmethod
    def add_project(self, project_data: Project) -> None:
        pass

    @abc.abstractmethod
    def delete_project(self, project: Project) -> None:
        pass

    @abc.abstractmethod
    def get_project(self, project_id: str) -> Project:
        pass

    @abc.abstractmethod
    def get_project_by_name(self, project_name: str) -> Project:
        pass
