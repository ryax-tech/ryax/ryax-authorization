# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.authorization.domain.base_command import BaseCommand
from ryax.authorization.domain.base_event import BaseEvent


@dataclass
class CreateProjectCommand(BaseCommand):
    attibute1: str


@dataclass
class ProjectIsReadyEvent(BaseEvent):
    id: str
