# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
from dataclasses import dataclass, field
from typing import List

from ryax.authorization.domain.project.project_values import ProjectUpdateData
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User


@dataclass
class Project:
    id: str
    name: str
    created_at: datetime.datetime
    users: List[User] = field(default_factory=list)

    def get_user(self, user_id: str) -> User:
        for user in self.users:
            if user.id == user_id:
                return user
        raise UserNotFoundException()

    def update_data(self, project_data: ProjectUpdateData) -> None:
        self.name = project_data.name

    def add_user_to_project(self, user: User) -> None:
        self.users.append(user)

    def delete_user_from_project(self, user_id: str) -> None:
        # WARNING: We use this instead of the `remove` methode because it fails with SqlAlchemy
        for index, user in enumerate(self.users):
            if user.id == user_id:
                del self.users[index]

    def remove_all_users(self) -> None:
        for user in self.users:
            if user.current_project == self:
                user.current_project = None
        self.users = []
