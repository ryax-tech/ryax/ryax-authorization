# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Optional

from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.user.user_object import User


class ISecurityService(abc.ABC):
    @abc.abstractmethod
    def get_auth_token(self, token: str) -> Optional[AuthToken]:
        """Method to check authorization token"""
        pass

    @abc.abstractmethod
    def create_auth_token(self, user: User) -> str:
        pass

    @abc.abstractmethod
    def check_password(self, user_password: str, password: str) -> bool:
        pass

    @abc.abstractmethod
    def create_user_password(self, password: str) -> str:
        pass
