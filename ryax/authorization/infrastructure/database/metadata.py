# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import (
    Column,
    DateTime,
    Enum,
    ForeignKey,
    MetaData,
    String,
    Table,
    UniqueConstraint,
)
from sqlalchemy.dialects.postgresql import UUID

from ryax.authorization.domain.user.user_values import UserRoles, UserStates

metadata = MetaData()

# Project table declaration
project_table: Table = Table(
    "project",
    metadata,
    Column("id", UUID, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("created_at", DateTime, nullable=False),
    UniqueConstraint("name"),
)

user_roles = Enum(
    UserRoles,
    name="user_role",
    values_callable=lambda x: [e.value for e in x],
)

user_states = Enum(
    UserStates,
    name="user_state",
    values_callable=lambda x: [e.value for e in x],
)

# User table declaration
user_table: Table = Table(
    "user",
    metadata,
    Column("id", UUID, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("password", String, nullable=False),
    Column("created_at", DateTime, nullable=False),
    Column("modified_at", DateTime, nullable=False),
    Column("role", user_roles, nullable=False),
    Column("state", user_states, nullable=False),
    Column("email", String, nullable=True),
    Column("comment", String, nullable=True),
    Column("current_project_id", UUID, ForeignKey("project.id"), nullable=True),
    UniqueConstraint("name"),
)

user_project_association = Table(
    "user_project_association",
    metadata,
    Column("user_id", UUID, ForeignKey("user.id"), primary_key=True),
    Column("project_id", UUID, ForeignKey("project.id"), primary_key=True),
)
