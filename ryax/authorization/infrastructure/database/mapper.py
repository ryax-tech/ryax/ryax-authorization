# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy.orm import mapper, relationship

from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.infrastructure.database.metadata import (
    project_table,
    user_project_association,
    user_table,
)


def start_mapping() -> None:
    """Method to start mapping between database and domain models"""

    mapper(
        User,
        user_table,
        properties={
            "id": user_table.columns.id,
            "username": user_table.columns.name,
            "password": user_table.columns.password,
            "role": user_table.columns.role,
            "state": user_table.columns.state,
            "email": user_table.columns.email,
            "comment": user_table.columns.comment,
            "current_project": relationship(Project),
        },
    )

    mapper(
        Project,
        project_table,
        properties={
            "id": project_table.columns.id,
            "name": project_table.columns.name,
            "users": relationship(
                User,
                secondary=user_project_association,
                lazy=False,
                backref="projects",
                passive_deletes=True,
            ),
        },
    )
