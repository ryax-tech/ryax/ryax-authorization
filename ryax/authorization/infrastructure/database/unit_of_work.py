# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Any, Callable

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.infrastructure.database.engine import DatabaseEngine
from ryax.authorization.infrastructure.database.repositories.project_repository import (
    DatabaseProjectRepository,
)
from ryax.authorization.infrastructure.database.repositories.user_repository import (
    DatabaseUserRepository,
)


class DatabaseUnitOfWork(IUnitOfWork):
    def __init__(
        self,
        engine: DatabaseEngine,
        project_repository_factory: Callable[..., DatabaseProjectRepository],
        user_repository_factory: Callable[..., DatabaseUserRepository],
    ):
        self.engine = engine
        self.project_repository_factory = project_repository_factory
        self.user_repository_factory = user_repository_factory

    def __enter__(self) -> IUnitOfWork:
        self.session = self.engine.get_session()
        self.projects = self.project_repository_factory(session=self.session)
        self.users = self.user_repository_factory(session=self.session)
        return self

    def __exit__(self, *args: Any) -> None:
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
        self.session.refresh()
