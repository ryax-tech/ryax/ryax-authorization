# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from sqlalchemy.orm import Session

from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_repository import IUserRepository
from ryax.authorization.domain.user.user_values import UserRoles


class DatabaseUserRepository(IUserRepository):
    """Postgres implementation of workflow repository"""

    def __init__(self, session: Session):
        self.session = session

    def add_user(self, user: User) -> None:
        self.session.add(user)

    def list_users(self) -> List[User]:
        return self.session.query(User).all()

    def get_user(self, user_id: str) -> User:
        user = self.session.query(User).get(user_id)
        if not user:
            raise UserNotFoundException()
        else:
            return user

    def delete_user(self, user: User) -> None:
        self.session.delete(user)

    def get_user_by_username(self, username: str) -> User:
        return self.session.query(User).filter(User.username == username).first()

    def get_user_projects(self, user: User) -> List[Project]:
        return self.session.query(Project).filter(Project.users.any(id=user.id)).all()  # type: ignore

    def number_of_admins(self) -> int:
        return self.session.query(User).filter(User.role == UserRoles.admin).count()
