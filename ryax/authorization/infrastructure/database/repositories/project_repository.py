# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from sqlalchemy.orm import Session

from ryax.authorization.domain.project.project_exceptions import (
    ProjectNotFoundException,
)
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_repository import IProjectRepository


class DatabaseProjectRepository(IProjectRepository):
    def __init__(self, session: Session):
        self.session = session

    def list_projects(self) -> List[Project]:
        return self.session.query(Project).all()

    def add_project(self, project: Project) -> None:
        self.session.add(project)

    def get_project(self, project_id: str) -> Project:
        project = self.session.query(Project).get(project_id)
        if not project:
            raise ProjectNotFoundException()
        else:
            return project

    def delete_project(self, project: Project) -> None:
        self.session.delete(project)

    def get_project_by_name(self, project_name: str) -> Project:
        return self.session.query(Project).filter(Project.name == project_name).first()
