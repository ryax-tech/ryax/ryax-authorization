# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
import logging
from typing import Optional

import jwt
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.services.security_service import ISecurityService
from ryax.authorization.domain.user.user_object import User

# Todo: Define how to use expiration time and pass this variable in app configurations
JWT_EXP_DELTA_SECONDS = 86400


class SecurityService(ISecurityService):
    def __init__(self, secret_key: str):
        self.logger: logging.Logger = logging.getLogger("AuthService")
        self.secret_key: str = secret_key
        self.algorithm: str = "HS256"
        self.expiration_time: int = JWT_EXP_DELTA_SECONDS

    def get_auth_token(self, token: str) -> Optional[AuthToken]:
        try:
            decoded_token = jwt.decode(
                jwt=token,
                key=self.secret_key,
                verify=True,
                algorithms=[self.algorithm],
            )
            self.logger.debug("Token decoded - %s", decoded_token)
            return AuthToken(user_id=decoded_token["user_id"])
        except jwt.DecodeError as err:
            self.logger.warning("Token decoding failed - %s", err)
            return None
        except jwt.ExpiredSignatureError as err:
            self.logger.warning("Token expired - %s", err)
            return None

    def create_auth_token(self, user: User) -> str:
        payload = {
            "user_id": user.id,
            "exp": datetime.datetime.now(datetime.timezone.utc)
            + datetime.timedelta(seconds=self.expiration_time),
            "role": user.role.value,
        }
        return jwt.encode(payload, self.secret_key, self.algorithm)

    def check_password(self, user_password: str, password: str) -> bool:
        return pbkdf2_sha256.verify(password, user_password)

    def create_user_password(self, password: Optional[str]) -> str:
        return pbkdf2_sha256.hash(password)
