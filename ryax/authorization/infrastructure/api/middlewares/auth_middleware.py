# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Callable, List, Union

from aiohttp import web
from aiohttp.web_response import Response, json_response
from dependency_injector.wiring import Provide

from ryax.authorization.application.auth_service import AuthService
from ryax.authorization.application.user_service import UserService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.infrastructure.api.schemas.error_schema import ErrorSchema


def skip() -> Callable:
    """
    This decorator may be placed on a controller method in
    order to skip authorization in the middleware_handler below.
    """

    def wrapper(handler: Callable) -> Callable:
        handler.auth_skip = True  # type: ignore
        return handler

    return wrapper


def handle(
    public_paths: List[str],
    admin_access: List[str],
    no_admin_access: List[str],
    service: AuthService = Provide[ApplicationContainer.auth_service],
    user_service: UserService = Provide[ApplicationContainer.user_service],
) -> Callable:
    @web.middleware
    async def middleware_handler(
        request: web.Request, handler: Callable
    ) -> Union[Callable, Response]:
        is_public = any(request.path.startswith(item) for item in public_paths)
        has_admin_access = any(request.path.startswith(item) for item in admin_access)
        has_no_admin_access = any(
            request.path.startswith(item) for item in no_admin_access
        )
        if is_public:
            return await handler(request)
        elif getattr(handler, "auth_skip", False):
            return await handler(request)
        else:
            token = request.headers.get("Authorization", "")
            token_payload = service.check_access(token)
            if not token_payload:
                result = ErrorSchema().dump({"error": "Access denied"})
                return json_response(result, status=401)
            else:
                request["user_id"] = token_payload.user_id
                if (
                    has_admin_access
                    and not has_no_admin_access
                    and not user_service.is_admin(request["user_id"])
                ):
                    result = ErrorSchema().dump({"error": "Access denied"})
                    return web.json_response(result, status=401)
                return await handler(request)

    return middleware_handler
