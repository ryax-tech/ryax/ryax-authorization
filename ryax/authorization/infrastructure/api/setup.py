# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware  # type: ignore

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.infrastructure.api.controllers import (
    authentication_controller,
    project_controller,
    user_controller,
    util_controller,
)
from ryax.authorization.infrastructure.api.middlewares import auth_middleware
from ryax.authorization.version import __version__


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            auth_middleware,
            user_controller,
            util_controller,
            project_controller,
            authentication_controller,
        ]
    )

    # Setup server middlewares
    app.middlewares.extend(
        [
            auth_middleware.handle(
                public_paths=[
                    "/docs",
                    "/static",
                    "/healthz",
                    "/login",
                ],  # public access
                admin_access=["/users", "/projects"],  # use for user admin access
                no_admin_access=[
                    "/projects/users"
                ],  # use this to prevent admin access if prefix used in admin_access
            ),
            validation_middleware,
        ]
    )

    # Configure api routing
    app.add_routes(
        [
            web.get("/healthz", util_controller.health_check, allow_head=False),
            web.get("/users", user_controller.list_users, allow_head=False),
            web.post("/users", user_controller.create_user),
            web.get("/users/{user_id}", user_controller.get_user, allow_head=False),
            web.put("/users/{user_id}", user_controller.update_user),
            web.delete("/users/{user_id}", user_controller.delete_user),
            web.get(
                "/projects/users/{user_id}/projects",
                project_controller.get_user_projects,
                allow_head=False,
            ),
            web.get(
                "/projects/users/{user_id}/current",
                project_controller.get_user_current_project,
                allow_head=False,
            ),
            web.post(
                "/projects/users/{user_id}/current",
                project_controller.set_user_current_project,
            ),
            web.get("/projects", project_controller.list_projects, allow_head=False),
            web.post("/projects", project_controller.create_project),
            web.get(
                "/projects/{project_id}",
                project_controller.get_project,
                allow_head=False,
            ),
            web.delete("/projects/{project_id}", project_controller.delete_project),
            web.put("/projects/{project_id}", project_controller.update_project),
            web.post(
                "/projects/{project_id}/user", project_controller.add_user_to_project
            ),
            web.delete(
                "/projects/{project_id}/user/{user_id}",
                project_controller.delete_user_from_project,
            ),
            web.post("/login", authentication_controller.login),
            web.get("/logout", authentication_controller.logout, allow_head=False),
            web.get("/me", authentication_controller.who_am_i, allow_head=False),
            web.put("/me", authentication_controller.change_me),
            web.get("/v2/projects", project_controller.v2_user_projects),
            web.post(
                "/v2/projects/current", project_controller.v2_set_user_current_project
            ),
        ]
    )

    # Configure api documentation
    setup_aiohttp_apispec(
        app,
        title="authorization",
        version=__version__,
        url="/docs/swagger.json",
        swagger_path="/docs",
        static_path="/static/swagger",
        securityDefinitions={
            "bearer": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header",
                "description": "Ryax token",
            }
        },
        security=[{"bearer": []}],
    )
