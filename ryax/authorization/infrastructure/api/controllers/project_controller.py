# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema, response_schema
from dependency_injector.wiring import Provide

from ryax.authorization.application.project_service import ProjectService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_exceptions import (
    ProjectAlreadyExistsException,
    ProjectNotFoundException,
)
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import (
    AddUserToProjectData,
    ProjectCreateData,
    ProjectUpdateData,
)
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_values import SetCurrentProjectData
from ryax.authorization.infrastructure.api.schemas.add_project_schema import (
    AddProjectRequestSchema,
    AddProjectResponseSchema,
)
from ryax.authorization.infrastructure.api.schemas.add_user_to_project_schema import (
    AddUserToProjectRequestSchema,
)
from ryax.authorization.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.authorization.infrastructure.api.schemas.project_schema import (
    ProjectDetailsSchema,
    ProjectSchema,
    ProjectWithCurrentSchema,
)
from ryax.authorization.infrastructure.api.schemas.set_current_project_schema import (
    SetCurrentProjectRequestSchema,
)
from ryax.authorization.infrastructure.api.schemas.update_project_schema import (
    UpdateProjectRequestSchema,
)


@docs(
    tags=["Projects"],
    summary="List projects",
    description="List all projects",
    responses={
        200: {
            "description": "Project list fetched successfully",
            "schema": ProjectSchema(many=True),
        },
    },
)
async def list_projects(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    projects = service.list_projects()
    result: list = ProjectSchema().dump(projects, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Projects"],
    summary="Get one project",
    description="Get the requested project by ID",
)
@response_schema(
    ProjectDetailsSchema, code=200, description="Project fetched successfully"
)
@response_schema(ErrorSchema, code=404, description="Project not found")
async def get_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        project_id = request.match_info["project_id"]
        project = service.get_project(project_id)
        result = ProjectDetailsSchema().dump(project)
        return json_response(result, status=200)
    except ProjectNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Projects"],
    summary="Delete a project",
    description="Delete a project by id",
    responses={
        200: {"description": "Project deleted successfully"},
        404: {"description": "Project not found", "schema": ErrorSchema},
    },
)
async def delete_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        project_id = request.match_info["project_id"]
        service.delete_project(project_id)
        return json_response(None, status=200)
    except ProjectNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Projects"],
    summary="Update a project",
    description="Update project data",
    responses={
        200: {"description": "Project updated successfully", "schema": ProjectSchema},
        404: {"description": "Project not found", "schema": ErrorSchema},
    },
)
@request_schema(UpdateProjectRequestSchema())
async def update_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        project_id = request.match_info["project_id"]
        project_data = ProjectUpdateData(**request["data"])
        project = service.update_project(project_id, project_data)
        result = ProjectSchema().dump(project)
        return json_response(result, status=200)
    except ProjectNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Projects"],
    summary="Create a project",
    description="Create a new project",
)
@request_schema(AddProjectRequestSchema())
@response_schema(
    AddProjectResponseSchema, code=201, description="Project created successfully"
)
@response_schema(ErrorSchema, code=400, description="Project name already used")
@response_schema(ErrorSchema, code=401, description="Authentication failed")
async def create_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        data = request["data"]
        create_project_data = ProjectCreateData(**data)
        project_id = service.create_project(create_project_data)
        result = AddProjectResponseSchema().dump({"project_id": project_id})
        return json_response(result, status=201)
    except ProjectAlreadyExistsException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Projects"],
    summary="Add user to project",
    description="Add user to project",
    responses={
        201: {"description": "User has been add successfully"},
        404: {
            "description": "Project not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(AddUserToProjectRequestSchema())
async def add_user_to_project(
    request: web.Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        project_id = request.match_info["project_id"]
        add_user_data = AddUserToProjectData(**request["data"])
        service.add_user_to_project(project_id, add_user_data)
        return json_response(None, status=201)
    except ProjectNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Projects"],
    summary="Delete user from project",
    description="Delete user from project",
    responses={
        200: {"description": "User has been deleted successfully"},
        404: {
            "description": "Project not found",
            "schema": ErrorSchema,
        },
    },
)
async def delete_user_from_project(
    request: web.Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        project_id = request.match_info["project_id"]
        user_id = request.match_info["user_id"]
        deleted_user_id = service.delete_user_from_project(project_id, user_id)
        return json_response(deleted_user_id, status=200)
    except ProjectNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["UserProjects"],
    summary="Get user projects",
    description="Get user's accessible projects",
)
@response_schema(
    ProjectSchema(many=True), code=200, description="User projects fetched successfully"
)
@response_schema(ErrorSchema, code=404, description="User not found")
async def get_user_projects(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        projects: List[Project] = service.get_user_projects(user_id)
        result = ProjectSchema().dump(projects, many=True)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["UserProjects"],
    summary="Get user projects with current one",
    description="Get user's accessible projects and highlight the one currently selected",
)
@response_schema(
    ProjectWithCurrentSchema(many=True),
    code=200,
    description="User projects fetched successfully",
)
async def v2_user_projects(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    user_id = request["user_id"]
    projects: List[Project] = service.get_user_projects(user_id)
    current_project: Optional[Project] = service.get_current_project(user_id)
    result = ProjectWithCurrentSchema().dump(projects, many=True)
    if current_project is not None:
        current_in_list = next(p for p in result if p["id"] == current_project.id)
        current_in_list["current"] = True
    return json_response(result, status=200)


@docs(
    tags=["UserProjects"],
    summary="Get current project",
    description="Get current project",
)
@response_schema(
    ProjectSchema, code=200, description="User current project fetched successfully"
)
@response_schema(ErrorSchema, code=404, description="User not found")
async def get_user_current_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        project = service.get_current_project(user_id)
        result = ProjectSchema().dump(project)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["UserProjects"],
    summary="Set current project",
    description="Set current project",
    responses={
        201: {"description": "User current project set successfully"},
        404: {
            "description": "User not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(SetCurrentProjectRequestSchema())
async def set_user_current_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        current_project_data = SetCurrentProjectData(**request["data"])
        service.set_current_project(user_id, current_project_data)
        return json_response(None, status=201)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["UserProjects"],
    summary="Set current project",
    description="Set current project",
    responses={201: {"description": "User current project set successfully"}},
)
@request_schema(SetCurrentProjectRequestSchema())
async def v2_set_user_current_project(
    request: Request,
    service: ProjectService = Provide[ApplicationContainer.project_service],
) -> Response:
    user_id = request["user_id"]
    current_project_data = SetCurrentProjectData(**request["data"])
    service.set_current_project(user_id, current_project_data)
    return json_response(None, status=201)
