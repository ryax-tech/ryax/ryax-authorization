# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid

from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema, response_schema
from dependency_injector.wiring import Provide

from ryax.authorization.application.user_service import UserService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.user.user_exceptions import (
    NoMoreAdminsIfDoneException,
    UserAlreadyExistsException,
    UserNotFoundException,
)
from ryax.authorization.domain.user.user_values import UserCreateData, UserUpdateData
from ryax.authorization.infrastructure.api.schemas.add_user_schema import (
    AddUserRequestSchema,
    AddUserResponseSchema,
)
from ryax.authorization.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.authorization.infrastructure.api.schemas.update_user_schema import (
    UpdateUserRequestSchema,
)
from ryax.authorization.infrastructure.api.schemas.user_schema import UserDetailsSchema


@docs(
    tags=["Users"],
    summary="Create a user",
    description="Create a new user",
)
@request_schema(AddUserRequestSchema())
@response_schema(
    AddUserResponseSchema, code=201, description="User created successfully"
)
async def create_user(
    request: Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    try:
        data = request["data"]
        user_data = UserCreateData(
            username=data["username"],
            password=data["password"],
            role=data["role"],
            email=data["email"],
            comment=data.get("comment", ""),
        )
        user_id = service.create_user(user_data)
        result = AddUserResponseSchema().dump({"user_id": user_id})
        return json_response(result, status=201)
    except UserAlreadyExistsException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=401)


@docs(
    tags=["Users"],
    summary="List users",
    description="Show all users",
    responses={
        200: {
            "description": "Users list fetched successfully",
            "schema": UserDetailsSchema(many=True),
        },
    },
)
async def list_users(
    request: Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    users = service.list_users()
    result: list = UserDetailsSchema().dump(users, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Users"],
    summary="Get one user",
    description="Get the requested user by ID",
)
@response_schema(UserDetailsSchema, code=200, description="User fetched successfully")
@response_schema(ErrorSchema, code=404, description="User not found")
async def get_user(
    request: Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        user = service.get_user(user_id)
        result = UserDetailsSchema().dump(user)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Users"],
    summary="Update a user",
    description="Update user data",
)
@request_schema(UpdateUserRequestSchema())
@response_schema(UserDetailsSchema, code=200, description="User updated successfully")
@response_schema(ErrorSchema, code=404, description="User not found")
@response_schema(
    ErrorSchema,
    code=400,
    description="Cannot perform this operation, otherwise Ryax will not have any administrators",
)
async def update_user(
    request: Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        user_data = UserUpdateData(**request["data"])
        user = service.update_user(user_id, user_data)
        result = UserDetailsSchema().dump(user)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except NoMoreAdminsIfDoneException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Users"],
    summary="Delete user",
    description="Delete a user with ID",
    responses={
        200: {"description": "Users list fetched successfully"},
    },
)
@response_schema(ErrorSchema, code=404, description="User not found")
@response_schema(ErrorSchema, code=400, description="Impossible to delete yourself.")
async def delete_user(
    request: Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    try:
        user_id = request.match_info["user_id"]
        if user_id == request["user_id"]:
            return json_response(
                ErrorSchema().dump({"error": "Impossible to delete yourself."}),
                status=400,
            )
        try:
            uuid.UUID(user_id)
        except ValueError:
            raise UserNotFoundException()
        service.delete_user(user_id)
        return json_response(None, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
