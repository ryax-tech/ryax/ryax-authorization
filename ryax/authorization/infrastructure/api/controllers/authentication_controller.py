# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema, response_schema
from dependency_injector.wiring import Provide

from ryax.authorization.application.auth_service import AuthService
from ryax.authorization.application.user_service import UserService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.auth.auth_exceptions import WrongCredentialsException
from ryax.authorization.domain.auth.auth_values import LoginData
from ryax.authorization.domain.user.user_exceptions import (
    NoMoreAdminsIfDoneException,
    RoleUpdateForbiddenError,
    UserNotFoundException,
)
from ryax.authorization.domain.user.user_values import UserUpdateData
from ryax.authorization.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.authorization.infrastructure.api.schemas.login_schema import (
    LoginRequestSchema,
    LoginResponseSchema,
)
from ryax.authorization.infrastructure.api.schemas.update_user_schema import (
    UpdateUserRequestSchema,
)
from ryax.authorization.infrastructure.api.schemas.user_schema import (
    UserDetailsSchema,
    UserSchema,
)


@docs(
    tags=["Auth"],
    summary="Login",
    description="Login",
)
@request_schema(LoginRequestSchema())
@response_schema(LoginResponseSchema, code=200, description="Login successful")
@response_schema(ErrorSchema, code=401, description="Access denied! Wrong credentials")
@response_schema(ErrorSchema, code=404, description="User not found")
async def login(
    request: web.Request,
    service: AuthService = Provide[ApplicationContainer.auth_service],
) -> Response:
    try:
        data = request["data"]
        login_data = LoginData(**data)
        jwt = service.login(login_data)
        result = LoginResponseSchema().dump({"jwt": jwt})
        return json_response(result, status=200)
    except WrongCredentialsException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=401)


@docs(
    tags=["Auth"],
    summary="Logout",
    description="Logout",
    responses={
        200: {"description": "Logout successfully"},
    },
)
async def logout(
    request: web.Request,
    service: AuthService = Provide[ApplicationContainer.auth_service],
) -> Response:
    return json_response(None, status=200)


@docs(
    tags=["Auth"],
    summary="Who i am",
    description="Auth me",
)
@response_schema(UserDetailsSchema, code=200, description="User fetched successfully")
@response_schema(ErrorSchema, code=404, description="User not found")
async def who_am_i(
    request: web.Request,
    service: AuthService = Provide[ApplicationContainer.auth_service],
) -> Response:
    try:
        user_id = request["user_id"]
        user = service.who_am_i(user_id)
        result = UserDetailsSchema().dump(user)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Auth"],
    summary="Change me",
    description="Change me",
    responses={
        200: {"description": "User updated successfully", "schema": UserSchema},
        404: {"description": "User not found", "schema": ErrorSchema},
    },
)
@request_schema(UpdateUserRequestSchema())
@response_schema(UserDetailsSchema, code=200, description="User updated successfully")
@response_schema(ErrorSchema, code=404, description="User not found")
@response_schema(
    ErrorSchema,
    code=400,
    description="Cannot perform this operation",
)
async def change_me(
    request: web.Request,
    service: UserService = Provide[ApplicationContainer.user_service],
) -> Response:
    try:
        user_id = request["user_id"]
        user_data = UserUpdateData(**request["data"])
        user = service.update_user(user_id, user_data)
        result = UserDetailsSchema().dump(user)
        return json_response(result, status=200)
    except UserNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except (NoMoreAdminsIfDoneException, RoleUpdateForbiddenError) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
