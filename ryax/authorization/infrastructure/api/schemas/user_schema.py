# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.authorization.domain.user.user_values import UserRoles


class UserSchema(Schema):
    id = fields.String(
        metadata={
            "description": "ID of the user",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )

    username = fields.String(
        metadata={
            "description": "Name of the user",
            "example": "User",
        },
    )


class UserDetailsSchema(UserSchema):
    role = fields.String(
        metadata={
            "description": "Type of the output",
            "example": "User",
            "enum": list(item.value for item in UserRoles),
        },
        attribute="role.value",
    )

    email = fields.Email(
        metadata={
            "description": "User's email address",
            "example": "user1@example.com",
        },
    )

    comment = fields.Str(
        metadata={
            "description": "A comment on the user",
            "example": "Testing user for project 1",
        },
    )
