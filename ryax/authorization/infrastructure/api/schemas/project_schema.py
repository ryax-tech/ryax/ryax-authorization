# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.authorization.infrastructure.api.schemas.user_schema import UserSchema


class ProjectSchema(Schema):
    id = fields.String(
        metadata={
            "description": "ID of the project",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )

    name = fields.String(
        metadata={
            "description": "Name of the project",
            "example": "Project1",
        },
    )

    creation_date = fields.DateTime(
        metadata={
            "description": "Creation date of the project",
            "example": "2020-10-28T16:05:19.031339",
        },
        attribute="created_at",
    )


class ProjectDetailsSchema(ProjectSchema):
    users = fields.Nested(
        UserSchema,
        metadata={
            "description": "Users assined to this project",
        },
        many=True,
    )


class ProjectWithCurrentSchema(ProjectSchema):
    current = fields.Bool(
        metadata={"description": "The user's current project"},
    )
