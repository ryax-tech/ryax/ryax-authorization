# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class UpdateProjectRequestSchema(Schema):
    name = fields.String(
        metadata={
            "description": "Project name",
            "example": "Project_name_1",
        },
        validate=[validate.Length(3, 128)],
    )
