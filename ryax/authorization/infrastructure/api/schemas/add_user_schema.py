# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate

from ryax.authorization.domain.user.user_values import UserRoles


class AddUserRequestSchema(Schema):
    username = fields.String(
        required=True,
        metadata={
            "description": "Username",
            "example": "user_1",
        },
        validate=[validate.Length(3, 128)],
    )

    password = fields.String(
        required=True,
        metadata={
            "description": "User's password",
            "example": "secret_password",
        },
        validate=[validate.Length(3, 128)],
        load_only=True,
    )

    role = fields.String(
        required=True,
        metadata={
            "description": "User's role",
            "example": "User",
            "enum": list(item.value for item in UserRoles),
        },
        validate=validate.OneOf(list(item.value for item in UserRoles)),
    )

    email = fields.Email(
        required=True,
        metadata={
            "description": "User's email address",
            "example": "user1@example.com",
        },
        validate=[validate.Length(0, 256)],
    )

    comment = fields.String(
        metadata={
            "description": "A comment on the user",
            "example": "Testing user for project 1",
        },
        validate=[validate.Length(0, 512)],
    )


class AddUserResponseSchema(Schema):
    user_id = fields.String(
        metadata={
            "description": "ID of created user",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )
