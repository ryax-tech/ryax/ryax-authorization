# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class AddProjectRequestSchema(Schema):
    name = fields.Str(
        required=True,
        metadata={
            "example": "project_1",
        },
        validate=[validate.Length(3, 128)],
    )


class AddProjectResponseSchema(Schema):
    project_id = fields.String(
        metadata={
            "description": "ID of created project",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )
