# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class LoginRequestSchema(Schema):
    username = fields.String(
        required=True,
        metadata={
            "example": "user1",
        },
        validate=[validate.Length(3, 128)],
    )
    password = fields.String(
        required=True,
        metadata={
            "example": "secret_password",
        },
        validate=[validate.Length(3, 128)],
        load_only=True,
    )


class LoginResponseSchema(Schema):
    jwt = fields.String(
        metadata={
            "description": "JSON Web Token for authentication",
            "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
        },
    )
