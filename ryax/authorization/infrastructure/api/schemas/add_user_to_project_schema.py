# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class AddUserToProjectRequestSchema(Schema):
    user_id = fields.String(
        required=True,
        metadata={
            "description": "User id to add to project",
            "example": "22353926-51f5-4f44-ae74-f171371d7546",
        },
    )
