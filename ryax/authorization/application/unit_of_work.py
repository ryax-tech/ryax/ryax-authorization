# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Any

from ryax.authorization.domain.project.project_repository import IProjectRepository
from ryax.authorization.domain.user.user_repository import IUserRepository


class IUnitOfWork(abc.ABC):
    projects: IProjectRepository
    users: IUserRepository

    def __enter__(self) -> "IUnitOfWork":
        return self

    def __exit__(self, *args: Any) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self) -> None:
        raise NotImplementedError
