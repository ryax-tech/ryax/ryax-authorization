# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from typing import List, Optional

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.domain.project.project_exceptions import (
    ProjectAlreadyExistsException,
)
from ryax.authorization.domain.project.project_factory import ProjectFactory
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import (
    AddUserToProjectData,
    ProjectCreateData,
    ProjectUpdateData,
)
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import SetCurrentProjectData


class ProjectService:
    def __init__(self, uow: IUnitOfWork, factory: ProjectFactory):
        self.uow = uow
        self.factory = factory
        self.logger = logging.getLogger(self.__class__.__name__)

    def list_projects(self) -> List[Project]:
        with self.uow:
            projects = self.uow.projects.list_projects()
        return projects

    def create_project(self, project_data: ProjectCreateData) -> str:
        with self.uow:
            check_project_name: Project = self.uow.projects.get_project_by_name(
                project_data.name
            )
            if check_project_name:
                raise ProjectAlreadyExistsException()
            project: Project = self.factory.from_create_project(project_data)
            self.uow.projects.add_project(project)
            self.uow.commit()
            project_id = project.id
        return project_id

    def get_project(self, project_id: str) -> Project:
        with self.uow:
            project: Project = self.uow.projects.get_project(project_id)
        return project

    def update_project(
        self, project_id: str, project_data: ProjectUpdateData
    ) -> Project:
        with self.uow:
            project: Project = self.uow.projects.get_project(project_id)
            project.update_data(project_data)
            self.uow.commit()
            updated_project: Project = self.uow.projects.get_project(project_id)
        return updated_project

    def delete_project(self, project_id: str) -> None:
        with self.uow:
            project = self.uow.projects.get_project(project_id)
            project.remove_all_users()
            self.uow.projects.delete_project(project)
            self.uow.commit()

    def add_user_to_project(
        self, project_id: str, add_user_data: AddUserToProjectData
    ) -> None:
        with self.uow:
            project: Project = self.uow.projects.get_project(project_id)
            user: User = self.uow.users.get_user(add_user_data.user_id)
            project.add_user_to_project(user)
            if user.current_project is None:
                user.set_current_project(project)
            self.uow.commit()

    def delete_user_from_project(self, project_id: str, user_id: str) -> str:
        with self.uow:
            project: Project = self.uow.projects.get_project(project_id)
            project.delete_user_from_project(user_id)
            self.uow.commit()
        return user_id

    def get_user_projects(self, user_id: str) -> List[Project]:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
            user_projects = self.uow.users.get_user_projects(user)
        return user_projects

    def get_current_project(self, user_id: str) -> Optional[Project]:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
            return user.current_project

    def set_current_project(
        self, user_id: str, current_project_data: SetCurrentProjectData
    ) -> None:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
            project: Project = self.uow.projects.get_project(
                current_project_data.project_id
            )
            user.set_current_project(project)
            self.uow.commit()
