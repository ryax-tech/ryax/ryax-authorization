# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.domain.auth.auth_exceptions import WrongCredentialsException
from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.auth.auth_values import LoginData
from ryax.authorization.domain.services.security_service import ISecurityService
from ryax.authorization.domain.user.user_object import User


class AuthService:
    def __init__(self, uow: IUnitOfWork, security_service: ISecurityService):
        self.security_service = security_service
        self.uow = uow

    def check_access(self, token: str) -> Optional[AuthToken]:
        return self.security_service.get_auth_token(token) if token else None

    def login(self, login_data: LoginData) -> str:
        with self.uow:
            user: User = self.uow.users.get_user_by_username(login_data.username)
            if not user:
                raise WrongCredentialsException
            if self.security_service.check_password(user.password, login_data.password):
                jwt: str = self.security_service.create_auth_token(user)
                return jwt
            else:
                raise WrongCredentialsException

    def who_am_i(self, user_id: str) -> User:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
            return user
