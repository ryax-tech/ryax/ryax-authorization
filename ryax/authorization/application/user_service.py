# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.domain.user.user_exceptions import (
    NoMoreAdminsIfDoneException,
    RoleUpdateForbiddenError,
    UserAlreadyExistsException,
    UserNotFoundException,
)
from ryax.authorization.domain.user.user_factory import UserFactory
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    UserCreateData,
    UserRoles,
    UserUpdateData,
)
from ryax.authorization.infrastructure.security.security_service import SecurityService


class UserService:
    def __init__(
        self, uow: IUnitOfWork, factory: UserFactory, security_service: SecurityService
    ):
        self.uow = uow
        self.factory = factory
        self.security_service = security_service

    def list_users(self) -> List[User]:
        with self.uow:
            users = self.uow.users.list_users()
        return users

    def create_user(self, user_data: UserCreateData) -> str:
        with self.uow:
            check_username: User = self.uow.users.get_user_by_username(
                user_data.username
            )
            if check_username:
                raise UserAlreadyExistsException()
            created_password = self.security_service.create_user_password(
                user_data.password
            )
            user: User = self.factory.from_create_user(user_data, created_password)
            self.uow.users.add_user(user)
            self.uow.commit()
            user_id = user.id
        return user_id

    def get_user(self, user_id: str) -> User:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
        return user

    def update_user(self, user_id: str, user_data: UserUpdateData) -> User:
        with self.uow:
            created_password = (
                self.security_service.create_user_password(user_data.password)
                if user_data.password
                else None
            )
            user: User = self.uow.users.get_user(user_id)
            if user_data.role is not None and user.role != UserRoles.admin:
                raise RoleUpdateForbiddenError()
            if (
                user_data.role is not None
                and user.role == UserRoles.admin
                and user_data.role != UserRoles.admin
                and self.uow.users.number_of_admins() <= 1
            ):
                raise NoMoreAdminsIfDoneException()

            user.update_data(user_data, created_password)
            self.uow.commit()
            updated_user: User = self.uow.users.get_user(user_id)
        return updated_user

    def delete_user(self, user_id: str) -> None:
        with self.uow:
            user: User = self.uow.users.get_user(user_id)
            self.uow.users.delete_user(user)
            self.uow.commit()

    def is_admin(self, user_id: str) -> bool:
        with self.uow:
            try:
                user: User = self.uow.users.get_user(user_id)
            except UserNotFoundException:
                return False
        return user.role == UserRoles.admin
