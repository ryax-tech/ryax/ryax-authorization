# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dependency_injector import containers, providers

from .application.auth_service import AuthService
from .application.project_service import ProjectService
from .application.unit_of_work import IUnitOfWork
from .application.user_service import UserService
from .application.util_service import UtilService
from .domain.project.project_factory import ProjectFactory
from .domain.project.project_repository import IProjectRepository
from .domain.user.user_factory import UserFactory
from .domain.user.user_repository import IUserRepository
from .infrastructure.database.engine import DatabaseEngine
from .infrastructure.database.repositories.project_repository import (
    DatabaseProjectRepository,
)
from .infrastructure.database.repositories.user_repository import DatabaseUserRepository
from .infrastructure.database.unit_of_work import DatabaseUnitOfWork
from .infrastructure.security.security_service import SecurityService


class ApplicationContainer(containers.DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration provider
    configuration = providers.Configuration()

    database_engine: providers.Singleton[DatabaseEngine] = providers.Singleton(
        DatabaseEngine, connection_url=configuration.ryax_datastore
    )

    security_service = providers.Singleton(
        SecurityService, secret_key=configuration.jwt_secret_key
    )

    util_service = providers.Singleton(UtilService)

    db_engine: providers.Singleton[DatabaseEngine] = providers.Singleton(
        DatabaseEngine, connection_url=configuration.ryax_datastore
    )

    project_repository: providers.Factory[IProjectRepository] = providers.Factory(
        DatabaseProjectRepository
    )

    user_repository: providers.Factory[IUserRepository] = providers.Factory(
        DatabaseUserRepository
    )

    unit_of_work: providers.Factory[IUnitOfWork] = providers.Factory(
        DatabaseUnitOfWork,
        engine=database_engine,
        project_repository_factory=project_repository.provider,
        user_repository_factory=user_repository.provider,
    )

    auth_service = providers.Singleton(
        AuthService, uow=unit_of_work, security_service=security_service
    )

    user_factory: providers.Singleton[UserFactory] = providers.Singleton(
        UserFactory,
    )
    project_factory: providers.Singleton[ProjectFactory] = providers.Singleton(
        ProjectFactory,
    )

    user_service = providers.Singleton(
        UserService,
        uow=unit_of_work,
        factory=user_factory,
        security_service=security_service,
    )
    project_service = providers.Singleton(
        ProjectService,
        uow=unit_of_work,
        factory=project_factory,
    )
