import datetime
from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.authorization.domain.project.project_exceptions import (
    ProjectAlreadyExistsException,
    ProjectNotFoundException,
)
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import (
    AddUserToProjectData,
    ProjectCreateData,
    ProjectUpdateData,
)
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    SetCurrentProjectData,
    UserRoles,
    UserStates,
)


async def test_list_projects(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    now = datetime.datetime.utcnow()
    project_1 = Project(id="project_1", name="Project 1", created_at=now)
    project_2 = Project(id="project_2", name="Project 2", created_at=now)
    project_service_mock.list_projects.return_value = [project_1, project_2]
    response = await api_client.get("/projects")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": "project_1",
            "name": "Project 1",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        },
        {
            "id": "project_2",
            "name": "Project 2",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        },
    ]


async def test_get_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    now = datetime.datetime.utcnow()
    user = User(
        id="userid1",
        username="username1",
        email="email",
        comment="comment",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        state=UserStates.ok,
        role=UserRoles.user,
    )
    project = Project(id=project_id, name="project_name", created_at=now, users=[user])
    project_service_mock.get_project.return_value = project
    response = await api_client.get(f"/projects/{project_id}")
    assert response.status == 200
    assert await response.json() == {
        "id": project.id,
        "name": project.name,
        "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        "users": [
            {
                "id": user.id,
                "username": user.username,
            }
        ],
    }
    project_service_mock.get_project.assert_called_once_with(project_id)


async def test_get_project_when_not_exists(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    project_service_mock.get_project.side_effect = ProjectNotFoundException()
    response = await api_client.get(f"/projects/{project_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Project not found"}
    project_service_mock.get_project.assert_called_once_with(project_id)


async def test_delete_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    project = Project(
        id="project_id",
        name="project name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project_service_mock.delete_project.return_value = project
    response = await api_client.delete(f"/projects/{project_id}")
    assert response.status == 200
    assert await response.json() is None
    project_service_mock.delete_project.assert_called_once_with(project_id)


async def test_delete_project_when_not_exists(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    project_service_mock.delete_project.side_effect = ProjectNotFoundException
    response = await api_client.delete(f"/projects/{project_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Project not found"}
    project_service_mock.delete_project.assert_called_once_with(project_id)


async def test_update_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    project_data = {"name": "Project updated name"}
    project = Project(
        id="project_id",
        name="Project updated name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project_service_mock.update_project.return_value = project
    response = await api_client.put(f"/projects/{project_id}", data=project_data)
    assert response.status == 200
    assert await response.json() == {
        "id": project.id,
        "name": project.name,
        "creation_date": "1970-01-01T00:00:00",
    }
    project_service_mock.update_project.assert_called_once_with(
        project_id,
        ProjectUpdateData(name=project_data["name"]),
    )


async def test_update_project_when_not_exists(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    project_data = {
        "name": "Project updated name",
    }
    project_service_mock.update_project.side_effect = ProjectNotFoundException()
    response = await api_client.put(f"/projects/{project_id}", data=project_data)
    assert response.status == 404
    assert await response.json() == {"error": "Project not found"}
    project_service_mock.update_project.assert_called_once_with(
        project_id,
        ProjectUpdateData(name=project_data["name"]),
    )


async def test_create_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_data = {"name": "Project name"}
    project_id = "project_id"
    project_service_mock.create_project = mock.MagicMock(return_value=project_id)
    response = await api_client.post("/projects", data=project_data)
    assert response.status == 201
    assert await response.json() == {"project_id": project_id}
    project_service_mock.create_project.assert_called_once_with(
        ProjectCreateData(
            name=project_data["name"],
        )
    )


async def test_create_project_when_name_user(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_data = {"name": "Project name"}
    project_service_mock.create_project.side_effect = ProjectAlreadyExistsException
    response = await api_client.post("/projects", data=project_data)
    assert response.status == 400
    assert await response.json() == {"error": "Project name already used"}
    project_service_mock.create_project.assert_called_once_with(
        ProjectCreateData(
            name=project_data["name"],
        )
    )


async def test_add_user_to_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    add_user_data = {"user_id": "user_id"}
    project_id = "project_id"
    project_service_mock.add_user_to_project = mock.MagicMock()
    response = await api_client.post(f"/projects/{project_id}/user", data=add_user_data)
    assert response.status == 201
    assert await response.json() is None
    project_service_mock.add_user_to_project.assert_called_once_with(
        project_id, AddUserToProjectData(**add_user_data)
    )


async def test_add_user_to_project_when_project_not_exists(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    add_user_data = {"user_id": "user_id"}
    project_id = "project_id"
    project_service_mock.add_user_to_project = mock.MagicMock(
        side_effect=ProjectNotFoundException
    )
    response = await api_client.post(f"/projects/{project_id}/user", data=add_user_data)
    assert response.status == 404
    assert await response.json() == {"error": "Project not found"}
    project_service_mock.add_user_to_project.assert_called_once_with(
        project_id, AddUserToProjectData(**add_user_data)
    )


async def test_add_user_to_project_when_user_not_exists(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    add_user_data = {"user_id": "user_id"}
    project_id = "project_id"
    project_service_mock.add_user_to_project = mock.MagicMock(
        side_effect=UserNotFoundException
    )
    response = await api_client.post(f"/projects/{project_id}/user", data=add_user_data)
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    project_service_mock.add_user_to_project.assert_called_once_with(
        project_id, AddUserToProjectData(**add_user_data)
    )


async def test_delete_user_from_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    user_id = "user_id"
    project_service_mock.delete_user_from_project.return_value = user_id
    response = await api_client.delete(f"/projects/{project_id}/user/{user_id}")
    assert response.status == 200
    assert await response.json() == user_id
    project_service_mock.delete_user_from_project.assert_called_once_with(
        project_id, user_id
    )


async def test_delete_user_from_project_when_project_not_found(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    user_id = "user_id"
    project_service_mock.delete_user_from_project = mock.MagicMock(
        side_effect=ProjectNotFoundException
    )
    response = await api_client.delete(f"/projects/{project_id}/user/{user_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Project not found"}
    project_service_mock.delete_user_from_project.assert_called_once_with(
        project_id, user_id
    )


async def test_delete_user_from_project_when_user_not_found(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    project_id = "project_id"
    user_id = "user_id"
    project_service_mock.delete_user_from_project = mock.MagicMock(
        side_effect=UserNotFoundException
    )
    response = await api_client.delete(f"/projects/{project_id}/user/{user_id}")
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    project_service_mock.delete_user_from_project.assert_called_once_with(
        project_id, user_id
    )


async def test_get_user_projects(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    now = datetime.datetime.utcnow()
    projects = [Project(id="project_1", name="Super project", created_at=now)]
    project_service_mock.get_user_projects.return_value = projects
    response = await api_client.get(f"/projects/users/{user_id}/projects")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": "project_1",
            "name": "Super project",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        }
    ]
    project_service_mock.get_user_projects.assert_called_with(user_id)


async def test_get_user_projects_when_user_not_found(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    project_service_mock.get_user_projects.side_effect = UserNotFoundException
    response = await api_client.get(f"/projects/users/{user_id}/projects")
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    project_service_mock.get_user_projects.assert_called_with(user_id)


async def test_get_user_current_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    now = datetime.datetime.utcnow()
    project = Project(id="project_1", name="Super project", created_at=now)
    project_service_mock.get_current_project.return_value = project
    response = await api_client.get(f"/projects/users/{user_id}/current")
    assert response.status == 200
    assert await response.json() == {
        "id": project.id,
        "name": project.name,
        "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
    }
    project_service_mock.get_current_project.assert_called_with(user_id)


async def test_get_user_current_project_when_user_not_found(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    project_service_mock.get_current_project.side_effect = UserNotFoundException
    response = await api_client.get(f"/projects/users/{user_id}/current")
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    project_service_mock.get_current_project.assert_called_with(user_id)


async def test_set_user_current_project(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    project_id = "project_id"
    current_project_data = {"project_id": project_id}
    project_service_mock.set_current_project = mock.MagicMock()
    response = await api_client.post(
        f"/projects/users/{user_id}/current", data=current_project_data
    )
    assert response.status == 201
    assert await response.json() is None
    project_service_mock.set_current_project.assert_called_with(
        user_id, SetCurrentProjectData(**current_project_data)
    )


async def test_set_user_current_project_when_user_not_found(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    project_id = "project_id"
    current_project_data = {"project_id": project_id}
    project_service_mock.set_current_project.side_effect = UserNotFoundException
    response = await api_client.post(
        f"/projects/users/{user_id}/current", data=current_project_data
    )
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    project_service_mock.set_current_project.assert_called_with(
        user_id, SetCurrentProjectData(**current_project_data)
    )


async def test_list_projects_v2(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    now = datetime.datetime.utcnow()
    project_1 = Project(id="project_1", name="Project 1", created_at=now)
    project_2 = Project(id="project_2", name="Project 2", created_at=now)
    project_service_mock.get_user_projects.return_value = [project_1, project_2]
    project_service_mock.get_current_project.return_value = project_2
    response = await api_client.get("/v2/projects")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": "project_1",
            "name": "Project 1",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        },
        {
            "id": "project_2",
            "name": "Project 2",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
            "current": True,
        },
    ]


async def test_list_projects_v2_without_current(
    project_service_mock: mock.MagicMock, api_client: TestClient
):
    now = datetime.datetime.utcnow()
    project_1 = Project(id="project_1", name="Project 1", created_at=now)
    project_2 = Project(id="project_2", name="Project 2", created_at=now)
    project_service_mock.get_user_projects.return_value = [project_1, project_2]
    project_service_mock.get_current_project.return_value = None
    response = await api_client.get("/v2/projects")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": "project_1",
            "name": "Project 1",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        },
        {
            "id": "project_2",
            "name": "Project 2",
            "creation_date": now.strftime("%Y-%m-%dT%H:%M:%S.%f"),
        },
    ]


async def test_v2_set_user_current_project(
    project_service_mock: mock.MagicMock,
    api_client: TestClient,
    auth_service_mock: mock.MagicMock,
):
    project_id = "project_id"
    current_project_data = {"project_id": project_id}
    project_service_mock.set_current_project = mock.MagicMock()
    response = await api_client.post("/v2/projects/current", data=current_project_data)
    assert response.status == 201
    assert await response.json() is None
    project_service_mock.set_current_project.assert_called_with(
        "user_id", SetCurrentProjectData(**current_project_data)
    )
