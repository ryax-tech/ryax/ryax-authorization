import datetime
from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.authorization.domain.user.user_exceptions import (
    UserAlreadyExistsException,
    UserNotFoundException,
)
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    UserCreateData,
    UserRoles,
    UserStates,
    UserUpdateData,
)


async def test_create_user(user_service_mock: mock.MagicMock, api_client: TestClient):
    user_id = "user_id"
    user_data = {
        "username": "username",
        "password": "super_secure_password",
        "role": "Admin",
        "email": "username@mail.com",
        "comment": "I am just a test.",
    }
    user_service_mock.create_user.return_value = user_id
    response = await api_client.post("/users", data=user_data)
    assert response.status == 201
    assert await response.json() == {"user_id": user_id}
    user_service_mock.create_user.assert_called_once_with(UserCreateData(**user_data))


async def test_create_user_when_already_exists(
    user_service_mock: mock.MagicMock, api_client: TestClient
):
    user_data = {
        "username": "username",
        "password": "super_secure_password",
        "role": "Admin",
        "email": "username@mail.com",
        "comment": "I am just a test.",
    }
    user_service_mock.create_user.side_effect = UserAlreadyExistsException
    response = await api_client.post("/users", data=user_data)
    assert response.status == 401
    assert await response.json() == {"error": UserAlreadyExistsException.message}
    user_service_mock.create_user.assert_called_once_with(UserCreateData(**user_data))


async def test_list_users(user_service_mock: mock.MagicMock, api_client: TestClient):
    user_1 = User(
        id="user_1",
        username="User 1",
        role=UserRoles.admin,
        email="user_1@mail.com",
        comment="No comment!",
        modified_at=datetime.datetime.utcnow(),
        created_at=datetime.datetime.utcnow(),
        password="password",
        state=UserStates.ok,
    )
    user_2 = User(
        id="user_2",
        username="User 2",
        role=UserRoles.developer,
        email="user_2@mail.com",
        modified_at=datetime.datetime.utcnow(),
        created_at=datetime.datetime.utcnow(),
        password="password",
        state=UserStates.ok,
        comment="",
    )
    user_service_mock.list_users.return_value = [user_1, user_2]
    response = await api_client.get("/users")
    assert response.status == 200
    assert await response.json() == [
        {
            "id": "user_1",
            "username": "User 1",
            "role": "Admin",
            "email": "user_1@mail.com",
            "comment": "No comment!",
        },
        {
            "id": "user_2",
            "username": "User 2",
            "role": "Developer",
            "email": "user_2@mail.com",
            "comment": "",
        },
    ]
    user_service_mock.list_users.assert_called_once()


async def test_get_user(user_service_mock: mock.MagicMock, api_client: TestClient):
    user_id = "user_id"
    user_1 = User(
        id="user_id",
        username="User 1",
        role=UserRoles.admin,
        email="user_1@mail.com",
        comment="No comment!",
        modified_at=datetime.datetime.utcnow(),
        created_at=datetime.datetime.utcnow(),
        password="password",
        state=UserStates.ok,
    )
    user_service_mock.get_user.return_value = user_1
    response = await api_client.get(f"/users/{user_id}")
    assert response.status == 200
    assert await response.json() == {
        "id": "user_id",
        "username": "User 1",
        "role": "Admin",
        "email": "user_1@mail.com",
        "comment": "No comment!",
    }
    user_service_mock.get_user.assert_called_with(user_id)


async def test_get_user_when_user_not_found(
    user_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    user_service_mock.get_user.side_effect = UserNotFoundException
    response = await api_client.get(f"/users/{user_id}")
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    user_service_mock.get_user.assert_called_with(user_id)


async def test_update_user(user_service_mock: mock.MagicMock, api_client: TestClient):
    user_id = "user_id"
    user_update_data = {
        "email": "username@mail.com",
        "password": "a_stronger_password",
        "comment": "testing!",
    }
    updated_user = User(
        id=user_id,
        username="super_user",
        role=UserRoles.admin,
        email=user_update_data["email"],
        comment=user_update_data["comment"],
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        state=UserStates.ok,
    )
    user_service_mock.update_user.return_value = updated_user
    response = await api_client.put(f"users/{user_id}", data=user_update_data)
    assert response.status == 200
    assert await response.json() == {
        "id": user_id,
        "username": updated_user.username,
        "role": updated_user.role.value,
        "email": updated_user.email,
        "comment": updated_user.comment,
    }
    user_service_mock.update_user.assert_called_once_with(
        user_id, UserUpdateData(**user_update_data)
    )


async def test_update_user_when_user_not_found(
    user_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    user_update_data = {"email": "username@mail.com", "password": "a_stronger_password"}
    user_service_mock.update_user.side_effect = UserNotFoundException
    response = await api_client.put(f"users/{user_id}", data=user_update_data)
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    user_service_mock.update_user.assert_called_once_with(
        user_id, UserUpdateData(**user_update_data)
    )
