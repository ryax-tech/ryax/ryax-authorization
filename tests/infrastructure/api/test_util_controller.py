from unittest import mock

import pytest
from aiohttp.test_utils import TestClient


@pytest.mark.parametrize(
    "is_healthy, status",
    [
        (True, 200),
        (False, 400),
    ],
)
async def test_service_is_healthy(
    util_service_mock: mock.MagicMock,
    api_client: TestClient,
    is_healthy: bool,
    status: int,
):
    util_service_mock.is_healthy.return_value = is_healthy
    response = await api_client.get("/healthz")
    assert response.status == status
    util_service_mock.is_healthy.assert_called_once()
