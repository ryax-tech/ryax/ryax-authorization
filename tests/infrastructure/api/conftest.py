from unittest import mock

import pytest
from aiohttp import web

from ryax.authorization.application.auth_service import AuthService
from ryax.authorization.application.project_service import ProjectService
from ryax.authorization.application.user_service import UserService
from ryax.authorization.application.util_service import UtilService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.infrastructure.api.middlewares import auth_middleware
from ryax.authorization.infrastructure.api.setup import setup as setup_api


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
async def api_client(app_container: ApplicationContainer, aiohttp_client):
    """Instantiate client to test api part"""
    app = web.Application()
    setup_api(app, app_container)
    # Redefine auth_middleware to remove admin access
    app.middlewares.pop(0)
    app.middlewares.extend(
        [
            auth_middleware.handle(
                public_paths=["/docs", "/static", "/healthz", "/login"],
                admin_access=[],
                no_admin_access=[],
            )
        ]
    )
    return await aiohttp_client(app)


@pytest.fixture()
def util_service_mock(app_container: ApplicationContainer):
    util_service_mock = mock.MagicMock(UtilService)
    app_container.util_service.override(util_service_mock)
    return util_service_mock


@pytest.fixture()
def auth_service_mock(app_container: ApplicationContainer):
    auth_service_mock = mock.MagicMock(AuthService)
    auth_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user_id")
    )
    app_container.auth_service.override(auth_service_mock)
    return auth_service_mock


@pytest.fixture()
def user_service_mock(app_container: ApplicationContainer, auth_service_mock):
    user_service_mock = mock.MagicMock(UserService)
    app_container.user_service.override(user_service_mock)
    return user_service_mock


@pytest.fixture()
def project_service_mock(app_container: ApplicationContainer, auth_service_mock):
    project_service_mock = mock.MagicMock(ProjectService)
    app_container.project_service.override(project_service_mock)
    return project_service_mock
