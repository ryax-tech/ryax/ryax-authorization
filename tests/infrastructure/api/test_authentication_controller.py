import datetime
from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.authorization.domain.auth.auth_exceptions import WrongCredentialsException
from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.auth.auth_values import LoginData
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import UserRoles, UserStates


async def test_login(auth_service_mock: mock.MagicMock, api_client: TestClient):
    login_data = {
        "username": "username",
        "password": "super_secure_password",
    }
    jwt_token = "jwt_token"
    auth_service_mock.login.return_value = jwt_token
    response = await api_client.post("/login", data=login_data)
    assert response.status == 200
    assert await response.json() == {"jwt": jwt_token}
    auth_service_mock.login.assert_called_once_with(LoginData(**login_data))


async def test_login_when_wrong_credentials(
    auth_service_mock: mock.MagicMock, api_client: TestClient
):
    login_data = {
        "username": "username",
        "password": "super_secure_password",
    }
    auth_service_mock.login.side_effect = WrongCredentialsException
    response = await api_client.post("/login", data=login_data)
    assert response.status == 401
    assert await response.json() == {"error": "Wrong credentials"}
    auth_service_mock.login.assert_called_once_with(LoginData(**login_data))


async def test_logout(auth_service_mock: mock.MagicMock, api_client: TestClient):
    response = await api_client.get("/logout")
    assert response.status == 200
    assert await response.json() is None


async def test_who_am_i(auth_service_mock: mock.MagicMock, api_client: TestClient):
    user_id = "user_id"
    user_1 = User(
        id=user_id,
        username="User 1",
        role=UserRoles.admin,
        email="user_1@mail.com",
        comment="No comment!",
        modified_at=datetime.datetime.utcnow(),
        created_at=datetime.datetime.utcnow(),
        password="password",
        state=UserStates.ok,
    )
    auth_service_mock.check_access.return_value = AuthToken(user_id=user_id)
    auth_service_mock.who_am_i.return_value = user_1
    response = await api_client.get("/me")
    assert response.status == 200
    assert await response.json() == {
        "id": "user_id",
        "username": "User 1",
        "role": "Admin",
        "email": "user_1@mail.com",
        "comment": "No comment!",
    }
    auth_service_mock.who_am_i.assert_called_with(user_id)


async def test_who_am_i_when_user_not_found(
    auth_service_mock: mock.MagicMock, api_client: TestClient
):
    user_id = "user_id"
    auth_service_mock.check_access.return_value = AuthToken(user_id=user_id)
    auth_service_mock.who_am_i.side_effect = UserNotFoundException
    response = await api_client.get("/me")
    assert response.status == 404
    assert await response.json() == {"error": "User not found"}
    auth_service_mock.who_am_i.assert_called_with(user_id)
