import datetime
import os
from typing import Callable

import pytest
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import clear_mappers, sessionmaker

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.user.user_values import UserRoles, UserStates
from ryax.authorization.infrastructure.database.engine import Session
from ryax.authorization.infrastructure.database.mapper import start_mapping
from ryax.authorization.infrastructure.database.metadata import metadata


@pytest.fixture()
def app_container(database_session: Session):
    container = ApplicationContainer()
    return container


@pytest.fixture(scope="function")
def database_engine():
    database_url = os.environ["RYAX_DATASTORE"]
    engine: Engine = create_engine(database_url)
    metadata.create_all(engine)
    start_mapping()
    yield engine
    clear_mappers()
    metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture(scope="function")
def database_session_factory(database_engine: Engine):
    return sessionmaker(bind=database_engine)


@pytest.fixture(scope="function")
def database_session(database_session_factory: Session):
    session = database_session_factory()
    yield session
    session.close()


@pytest.fixture(scope="function")
def generate_project(database_session: Session) -> Callable:
    def _generate(id=None, name=None, created_at=None) -> dict:
        project_raw = {
            "id": id,
            "name": name,
            "created_at": created_at,
        }
        database_session.execute(
            "INSERT INTO project (id, name, created_at)"
            "VALUES (:id, :name, :created_at)",
            project_raw,
        )
        return project_raw

    return _generate


@pytest.fixture(scope="function")
def generate_user(database_session: Session) -> Callable:
    def _generate(
        id: str = None,
        username: str = None,
        password: str = None,
        created_at: datetime = None,
        modified_at: datetime = None,
        role: UserRoles = None,
        state: UserStates = None,
        email: str = None,
        comment: str = None,
        current_project: Project = None,
    ) -> dict:
        user_raw = {
            "id": id,
            "username": username,
            "password": password,
            "created_at": created_at,
            "modified_at": modified_at,
            "role": role,
            "state": state,
            "email": email,
            "comment": comment,
        }
        database_session.execute(
            "INSERT INTO public.user (id, name, password, created_at, modified_at, role, state, email, comment)"
            "VALUES (:id, :username, :password, :created_at, :modified_at, :role, :state, :email, :comment)",
            user_raw,
        )
        return user_raw

    return _generate


@pytest.fixture(scope="function")
def generate_user_project_association(database_session: Session):
    def _generate(user_id: str, project_id: str):
        association = {
            "user_id": user_id,
            "project_id": project_id,
        }
        database_session.execute(
            "INSERT INTO user_project_association (user_id, project_id)"
            "VALUES (:user_id, :project_id )",
            association,
        )
        return association

    return _generate
