import datetime
from typing import Callable
from uuid import UUID

import pytest

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import UserRoles, UserStates
from ryax.authorization.infrastructure.database.engine import Session
from ryax.authorization.infrastructure.database.repositories.user_repository import (
    DatabaseUserRepository,
)


def test_list_users(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_user: Callable,
):
    user_1: dict = generate_user(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        username="username",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role="Admin",
        state="OK",
        email="email",
        comment="comment",
    )
    user_2: dict = generate_user(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        username="username_2",
        password="password_2",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role="Admin",
        state="OK",
        email="email_2",
        comment="comment_2",
    )

    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    result = user_repository.list_users()
    assert set([user.id for user in result]) == {user_1["id"], user_2["id"]}


def test_create_user(
    app_container: ApplicationContainer,
    database_session: Session,
):
    new_user = User(
        id="608f5b39-f383-40f7-8c1b-cac79e7324f0",
        username="user1",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role=UserRoles.developer,
        state=UserStates.ok,
        email="email",
        comment="comment",
    )
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    user_repository.add_user(new_user)
    database_session.commit()

    assert list(database_session.execute("SELECT id, name FROM public.user")) == [
        (
            UUID(new_user.id),
            new_user.username,
        )
    ]


def test_get_user(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_user: Callable,
):
    user_raw: dict = generate_user(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        username="username",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role="Admin",
        state="OK",
        email="email",
        comment="comment",
    )
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    result = user_repository.get_user(user_raw["id"])
    assert result.id == user_raw["id"]


def test_get_user_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    user_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    with pytest.raises(UserNotFoundException):
        user_repository.get_user(user_id)


def test_delete_user(app_container: ApplicationContainer, database_session: Session):
    raw_user = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f0",
        "username": "user 1",
        "password": "password",
        "created_at": datetime.datetime.utcnow(),
        "modified_at": datetime.datetime.utcnow(),
        "role": "User",
        "state": "OK",
        "email": "email",
        "comment": "comment",
    }
    database_session.execute(
        "INSERT INTO public.user (id, name, password, created_at, modified_at, role, state, email, comment) "
        "VALUES (:id, :username, :password, :created_at, :modified_at, :role, :state, :email, :comment)",
        raw_user,
    )
    user = database_session.query(User).get(raw_user["id"])
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    user_repository.delete_user(user)
    database_session.commit()

    assert list(database_session.execute("SELECT * FROM public.user")) == []


def test_get_user_by_username(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_user: Callable,
):
    user_raw: dict = generate_user(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        username="username",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role="Admin",
        state="OK",
        email="email",
        comment="comment",
    )
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    result = user_repository.get_user_by_username(user_raw["username"])
    assert result.id == user_raw["id"]


def test_get_user_by_username_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    user_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    assert user_repository.get_user_by_username(user_id) is None


def test_get_user_projects(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_user: Callable,
    generate_project: Callable,
    generate_user_project_association: Callable,
):
    user_raw: dict = generate_user(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        username="username",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role="Admin",
        state="OK",
        email="email",
        comment="comment",
    )
    project_raw: dict = generate_project(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Project 1",
        created_at=datetime.datetime.utcnow(),
    )
    generate_user_project_association(
        user_id=user_raw["id"],
        project_id=project_raw["id"],
    )
    user_repository: DatabaseUserRepository = app_container.user_repository(
        session=database_session
    )
    user = database_session.query(User).get(user_raw["id"])
    result = user_repository.get_user_projects(user)
    assert result == [Project(**project_raw, users=[user])]
