import datetime
from typing import Callable
from uuid import UUID

import pytest

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_exceptions import (
    ProjectNotFoundException,
)
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.infrastructure.database.engine import Session
from ryax.authorization.infrastructure.database.repositories.project_repository import (
    DatabaseProjectRepository,
)


def test_list_projects(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project: Callable,
):
    project_1: dict = generate_project(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Project name",
        created_at=datetime.datetime.utcnow(),
    )

    project_2: dict = generate_project(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Project name 2",
        created_at=datetime.datetime.utcnow(),
    )

    project_repository: DatabaseProjectRepository = app_container.project_repository(
        session=database_session
    )
    result = project_repository.list_projects()
    assert set([project.id for project in result]) == {project_1["id"], project_2["id"]}


def test_add_project(
    app_container: ApplicationContainer,
    database_session: Session,
):
    new_project = Project(
        id="608f5b39-f383-40f7-8c1b-cac79e7324f0",
        name="Project1",
        created_at=datetime.datetime.utcnow(),
    )
    project_repository: DatabaseProjectRepository = app_container.project_repository(
        session=database_session
    )
    project_repository.add_project(new_project)
    database_session.commit()

    assert list(
        database_session.execute("SELECT id, name, created_at FROM project")
    ) == [
        (
            UUID(new_project.id),
            new_project.name,
            new_project.created_at,
        )
    ]


def test_get_project(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project: Callable,
):
    project_raw: dict = generate_project(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Project 1",
        created_at=datetime.datetime.utcnow(),
    )
    project_repository: DatabaseProjectRepository = app_container.project_repository(
        session=database_session
    )
    result = project_repository.get_project(project_raw["id"])
    assert result.id == project_raw["id"]


def test_get_project_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    project_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    project_repository: DatabaseProjectRepository = app_container.project_repository(
        session=database_session
    )
    with pytest.raises(ProjectNotFoundException):
        project_repository.get_project(project_id)


def test_delete_project(app_container: ApplicationContainer, database_session: Session):
    raw_project = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f0",
        "name": "Project 1",
        "created_at": datetime.datetime.utcnow(),
    }
    database_session.execute(
        "INSERT INTO project (id, name, created_at) VALUES (:id, :name, :created_at)",
        raw_project,
    )
    project = database_session.query(Project).get(raw_project["id"])
    project_repository: DatabaseProjectRepository = app_container.project_repository(
        session=database_session
    )
    project_repository.delete_project(project)
    database_session.commit()

    assert (
        list(database_session.execute("SELECT id, name, created_at FROM project")) == []
    )
