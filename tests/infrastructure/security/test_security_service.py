# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import datetime
from unittest import mock

import jwt
import pytest

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import UserRoles, UserStates
from ryax.authorization.infrastructure.security.security_service import SecurityService


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@mock.patch("ryax.authorization.infrastructure.security.security_service.jwt.decode")
def test_get_auth_token(jwt_decode_mock, app_container: ApplicationContainer):
    user_id = "user_id"
    jwt_decode_mock.return_value = {"user_id": user_id}
    security_service: SecurityService = app_container.security_service()
    security_service.secret_key = "secret_key"
    jwt_token = "jwt_token"
    assert security_service.get_auth_token(jwt_token) == AuthToken(user_id)
    jwt_decode_mock.assert_called_once_with(
        jwt=jwt_token,
        key=security_service.secret_key,
        verify=True,
        algorithms=[security_service.algorithm],
    )


@mock.patch("ryax.authorization.infrastructure.security.security_service.jwt.decode")
def test_get_auth_token_when_invalid(
    jwt_decode_mock, app_container: ApplicationContainer
):
    jwt_decode_mock.side_effect = jwt.DecodeError()
    security_service: SecurityService = app_container.security_service()
    security_service.secret_key = "secret_key"
    jwt_token = "jwt_token"
    assert security_service.get_auth_token(jwt_token) is None
    jwt_decode_mock.assert_called_once_with(
        jwt=jwt_token,
        key=security_service.secret_key,
        verify=True,
        algorithms=[security_service.algorithm],
    )


@mock.patch("ryax.authorization.infrastructure.security.security_service.jwt.decode")
def test_get_auth_token_when_expired(
    jwt_decode_mock, app_container: ApplicationContainer
):
    jwt_decode_mock.side_effect = jwt.ExpiredSignatureError()
    security_service: SecurityService = app_container.security_service()
    security_service.secret_key = "secret_key"
    jwt_token = "jwt_token"
    assert security_service.get_auth_token(jwt_token) is None
    jwt_decode_mock.assert_called_once_with(
        jwt=jwt_token,
        key=security_service.secret_key,
        verify=True,
        algorithms=[security_service.algorithm],
    )


@mock.patch("ryax.authorization.infrastructure.security.security_service.datetime")
@mock.patch("ryax.authorization.infrastructure.security.security_service.jwt.encode")
def test_create_auth_token(
    jwt_encode_mock, datetime_mock, app_container: ApplicationContainer
):
    user = User(
        id="user_id",
        role=UserRoles.developer,
        current_project=Project(
            id="project_id",
            name="name",
            created_at=datetime.datetime.utcfromtimestamp(0),
        ),
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        state=UserStates.ok,
        email="email",
        comment="",
    )
    exp = datetime.datetime.utcnow()
    datetime_mock.datetime.now = mock.MagicMock(return_value=exp)
    datetime_mock.timedelta.return_value = datetime.timedelta(0)
    jwt_token = "jwt_token"
    jwt_encode_mock.return_value = jwt_token
    security_service: SecurityService = app_container.security_service()
    security_service.secret_key = "secret_key"
    assert security_service.create_auth_token(user) == jwt_token
    jwt_encode_mock.assert_called_once_with(
        {
            "user_id": user.id,
            "exp": exp,
            "role": user.role.value,
        },
        security_service.secret_key,
        security_service.algorithm,
    )


@mock.patch("ryax.authorization.infrastructure.security.security_service.pbkdf2_sha256")
def test_check_password(pbkdf2_sha256_mock, app_container: ApplicationContainer):
    user_password = "user_password"
    test_password = "test_password"
    pbkdf2_sha256_mock.verify.return_value = True
    security_service: SecurityService = app_container.security_service()
    assert security_service.check_password(user_password, test_password) is True
    pbkdf2_sha256_mock.verify.assert_called_once_with(test_password, user_password)


@mock.patch("ryax.authorization.infrastructure.security.security_service.pbkdf2_sha256")
def test_create_user_password(pbkdf2_sha256_mock, app_container: ApplicationContainer):
    user_password = "user_password"
    password_hash = "password_hash"
    pbkdf2_sha256_mock.hash.return_value = password_hash
    security_service: SecurityService = app_container.security_service()
    assert security_service.create_user_password(user_password) == password_hash
    pbkdf2_sha256_mock.hash.assert_called_once_with(user_password)
