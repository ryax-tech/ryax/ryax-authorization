import os

import pytest
import sqlalchemy
from aiohttp import test_utils, web
from sqlalchemy.orm import clear_mappers
from sqlalchemy.orm.session import _SessionClassMethods

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.infrastructure.api.setup import setup
from ryax.authorization.infrastructure.database.engine import DatabaseEngine
from ryax.authorization.infrastructure.database.metadata import metadata


@pytest.fixture()
async def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    app_container.configuration.set("jwt_secret_key", "secret_key_for_jwt")
    yield app_container
    app_container.unwire()


@pytest.fixture()
def api_client(
    loop, app_container: ApplicationContainer, aiohttp_client
) -> test_utils.TestClient:
    app = web.Application()
    setup(app, app_container)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> DatabaseEngine:
    database_url = os.environ["RYAX_DATASTORE"]
    engine = DatabaseEngine(database_url)
    engine.connect()
    app_container.database_engine.override(engine)
    yield engine
    clear_mappers()
    metadata.drop_all(engine.connection)
    engine.disconnect()


@pytest.fixture()
def database_session(database_engine: DatabaseEngine) -> _SessionClassMethods:
    session = database_engine.get_session()
    yield session
    sqlalchemy.orm.close_all_sessions()
