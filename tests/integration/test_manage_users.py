import datetime

import pytest
from aiohttp import test_utils
from marshmallow import RAISE
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import UserRoles, UserStates
from ryax.authorization.infrastructure.api.schemas.user_schema import UserDetailsSchema
from ryax.authorization.infrastructure.database.engine import Session


async def test_check_public_accesses(
    app_container: ApplicationContainer,
    database_session: Session,
    api_client: test_utils.TestClient,
) -> None:
    user1 = User(
        id="0724a25e-f7f2-4f29-9620-ea4647a90369",
        username="username",
        password=pbkdf2_sha256.hash("password"),
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role=UserRoles.admin,
        state=UserStates.ok,
        email="user@email.com",
        comment="comment",
    )
    database_session.add(user1)
    database_session.commit()

    response = await api_client.get("/healthz")
    assert response.status == 200
    response = await api_client.get("/users")
    assert response.status == 401
    response = await api_client.post("/users", data={})
    assert response.status == 401
    response = await api_client.get("/users/non_existing")
    assert response.status == 401
    response = await api_client.put("/users/non_existing", data={})
    assert response.status == 401
    response = await api_client.delete("/users/non_existing")
    assert response.status == 401
    response = await api_client.get("/projects/users/non_existing/projects")
    assert response.status == 401
    response = await api_client.get("/projects/users/non_existing/current")
    assert response.status == 401
    response = await api_client.post("/projects/users/non_existing/current", data={})
    assert response.status == 401
    response = await api_client.get("/projects")
    assert response.status == 401
    response = await api_client.post("/projects", data={})
    assert response.status == 401
    response = await api_client.get("/projects/{project_id}")
    assert response.status == 401
    response = await api_client.delete("/projects/{project_id}")
    assert response.status == 401
    response = await api_client.put("/projects/{project_id}", data={})
    assert response.status == 401
    response = await api_client.post("/projects/{project_id}/user", data={})
    assert response.status == 401
    response = await api_client.delete("/projects/{project_id}/user/{user_id}")
    assert response.status == 401
    response = await api_client.post("/login", data={})
    assert response.status == 422
    response = await api_client.get("/logout", data={})
    assert response.status == 401
    response = await api_client.get("/me")
    assert response.status == 401
    response = await api_client.put("/me", data={})
    assert response.status == 401
    response = await api_client.get("/v2/projects")
    assert response.status == 401
    response = await api_client.post("/v2/projects/current", data={})
    assert response.status == 401


async def test_no_admin_scenario(
    app_container: ApplicationContainer,
    database_session: Session,
    api_client: test_utils.TestClient,
) -> None:
    user1_id = "00000000-f7f2-4f29-9620-000000000000"
    user1 = User(
        id=user1_id,
        username="username",
        password=pbkdf2_sha256.hash("password"),
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role=UserRoles.admin,
        state=UserStates.ok,
        email="user@email.com",
        comment="comment",
    )
    database_session.add(user1)
    database_session.commit()

    response = await api_client.post(
        "/login", data={"username": "username", "password": "password"}
    )
    assert response.status == 200
    result = await response.json()
    jwt = result["jwt"]

    response = await api_client.get("/users", headers={"Authorization": jwt})
    assert response.status == 200
    result = await response.json()
    UserDetailsSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert len(result) == 1

    response = await api_client.post(
        "/users",
        data={
            "username": "new_user",
            "password": "new_user_password",
            "role": "Admin",
            "email": "test@example.org",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 201

    response = await api_client.get("/users", headers={"Authorization": jwt})
    assert response.status == 200
    result = await response.json()
    UserDetailsSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert len(result) == 2
    new_user_id = result[0]["id"]
    if new_user_id == user1_id:
        new_user_id = result[1]["id"]

    response = await api_client.put(
        f"/users/{new_user_id}",
        data={
            "role": "User",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 200

    response = await api_client.delete(
        f"/users/{user1_id}", headers={"Authorization": jwt}
    )
    assert response.status == 400
    response = await api_client.delete(
        f"/users/{new_user_id}", headers={"Authorization": jwt}
    )
    assert response.status == 200

    response = await api_client.put(
        f"/users/{user1_id}",
        data={
            "role": "User",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 400
    response = await api_client.get(
        f"/users/{user1_id}", headers={"Authorization": jwt}
    )
    assert response.status == 200
    result = await response.json()
    assert result["role"] == "Admin"

    response = await api_client.put(
        "/me",
        data={
            "role": "User",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 400
    response = await api_client.get(
        f"/users/{user1_id}", headers={"Authorization": jwt}
    )
    assert response.status == 200
    result = await response.json()
    assert result["role"] == "Admin"

    response = await api_client.delete(
        "/users/11111111-f7f2-4f29-9621-111111111111", headers={"Authorization": jwt}
    )
    assert response.status == 404


@pytest.mark.parametrize(
    "user_role", [*[role for role in UserRoles if role != UserRoles.admin]]
)
async def test_not_admin_user_permission(
    app_container: ApplicationContainer,
    database_session: Session,
    api_client: test_utils.TestClient,
    user_role: UserRoles,
) -> None:
    user1_id = "00000000-f7f2-4f29-9620-000000000000"
    user1 = User(
        id=user1_id,
        username="username",
        password=pbkdf2_sha256.hash("password"),
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role=user_role,
        state=UserStates.ok,
        email="user@email.com",
        comment="comment",
    )
    database_session.add(user1)
    database_session.commit()

    response = await api_client.post(
        "/login", data={"username": "username", "password": "password"}
    )
    assert response.status == 200
    result = await response.json()
    jwt = result["jwt"]

    response = await api_client.get("/projects", headers={"Authorization": jwt})
    assert response.status == 401
    response = await api_client.get("/users", headers={"Authorization": jwt})
    assert response.status == 401

    response = await api_client.post(
        "/users",
        data={
            "username": "new_user",
            "password": "new_user_password",
            "role": "Admin",
            "email": "test@example.org",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 401
    assert len(database_session.query(User).all()) == 1

    response = await api_client.put(
        f"/users/{user1_id}",
        data={
            "role": "Admin",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 401
    assert database_session.query(User).get(user1.id).role == user_role

    response = await api_client.put(
        "/me",
        data={
            "role": "Admin",
        },
        headers={"Authorization": jwt},
    )
    assert response.status == 400
    assert database_session.query(User).get(user1.id).role == user_role
