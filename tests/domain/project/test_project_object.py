import datetime

import pytest

from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import ProjectUpdateData
from ryax.authorization.domain.user.user_exceptions import UserNotFoundException
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import UserRoles, UserStates


def test_get_user():
    user_1 = User(
        id="user_1_id",
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_2 = User(
        id="user_2_id",
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    project = Project(
        id="project_id",
        name="name",
        users=[user_1, user_2],
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    assert project.get_user("user_1_id") == user_1


def test_get_user_when_not_found():
    project = Project(
        id="project_id",
        name="name",
        users=[],
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    with pytest.raises(UserNotFoundException):
        assert project.get_user("user_1_id")


def test_update_data():
    new_name = "project_new_name"
    project_update_data = ProjectUpdateData(name=new_name)
    project = Project(
        id="project_id",
        name="project_name",
        users=[],
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project.update_data(project_update_data)
    assert project.name == new_name


def test_add_user():
    user = User(
        id="user_id",
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    project = Project(
        id="project_id",
        name="project_name",
        users=[],
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project.add_user_to_project(user)
    assert project.users == [user]


def test_remove_all_users():
    project = Project(
        id="project_id",
        name="project_name",
        users=[],
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    user_1 = User(
        id="user_id",
        current_project=project,
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_2 = User(
        id="user_id",
        password="password",
        username="username",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    project.users.extend([user_1, user_2])
    project.remove_all_users()
    assert project.users == []
    assert user_1.current_project is None
