import datetime
from unittest import mock

from ryax.authorization.domain.project.project_factory import ProjectFactory
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_values import ProjectCreateData


@mock.patch("ryax.authorization.domain.project.project_factory.datetime")
def test_from_create_project(datetime_mock):
    project_factory = ProjectFactory()
    project_name = "project_name"
    project_data = ProjectCreateData(name="project_name", id="id")
    now = datetime.datetime.now()
    datetime_mock.datetime.now.return_value = now
    created_project = project_factory.from_create_project(project_data)
    assert created_project == Project(
        id=created_project.id, name=project_name, created_at=now
    )
