import datetime
from unittest import mock

from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    UserRoles,
    UserStates,
    UserUpdateData,
)


@mock.patch("ryax.authorization.domain.user.user_object.datetime")
def test_update_data(datetime_mock: mock.Mock) -> None:
    datetime_mock.datetime.now = mock.Mock(
        return_value=datetime.datetime(2021, 6, 25, 12, 18, 49, 135822)
    )
    user = User(
        id="new_id",
        username="username",
        password="password",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        role=UserRoles.user,
        state=UserStates.ok,
        email="user@email.com",
        comment="comment",
    )
    user_data = UserUpdateData(
        username="new_username",
        password="new_password",
        role=UserRoles.developer,
        email="new_user@email.com",
        comment="new_comment",
    )
    new_password = "new_password"
    user.update_data(user_data, new_password)
    assert user_data.username is not None
    assert user_data.role is not None
    assert user_data.email is not None
    assert user_data.comment is not None
    assert user == User(
        id=user.id,
        username=user_data.username,
        password=new_password,
        created_at=user.created_at,
        modified_at=datetime.datetime(2021, 6, 25, 12, 18, 49, 135822),
        role=user_data.role,
        state=user.state,
        email=user_data.email,
        comment=user_data.comment,
    )
