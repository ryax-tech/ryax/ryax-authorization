import datetime
from unittest import mock

from ryax.authorization.domain.user.user_factory import UserFactory
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_values import (
    UserCreateData,
    UserRoles,
    UserStates,
)


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_id"))
@mock.patch("ryax.authorization.domain.user.user_factory.datetime")
def test_from_create_user(datetime_mock):
    user_factory = UserFactory()
    user_data = UserCreateData(
        username="username",
        password="password",
        role=UserRoles.user,
        email="user@email.com",
        comment="comment",
    )
    created_password = "created_password"
    datetime_mock.datetime.now = mock.Mock(
        return_value=datetime.datetime(2021, 6, 25, 12, 18, 49, 135822)
    )
    created_user = user_factory.from_create_user(user_data, created_password)
    assert created_user == User(
        id="new_id",
        username=user_data.username,
        password=created_password,
        created_at=datetime.datetime(2021, 6, 25, 12, 18, 49, 135822),
        modified_at=datetime.datetime(2021, 6, 25, 12, 18, 49, 135822),
        role=UserRoles(user_data.role),
        state=UserStates.ok,
        email=user_data.email,
        comment=user_data.comment,
    )
