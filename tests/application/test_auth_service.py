import datetime
from unittest import mock

import pytest

from ryax.authorization.application.auth_service import AuthService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.auth.auth_exceptions import WrongCredentialsException
from ryax.authorization.domain.auth.auth_token import AuthToken
from ryax.authorization.domain.auth.auth_values import LoginData
from ryax.authorization.domain.services.security_service import ISecurityService
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_repository import IUserRepository
from ryax.authorization.domain.user.user_values import UserRoles, UserStates


def test_check_access(app_container: ApplicationContainer):
    auth_service: AuthService = app_container.auth_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.get_auth_token.return_value = AuthToken(user_id="user")
    result = auth_service.check_access("token")
    assert result == AuthToken(user_id="user")


def test_check_access_with_empty_token(app_container: ApplicationContainer):
    auth_service: AuthService = app_container.auth_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.get_auth_token.return_value = AuthToken(user_id="user")
    result = auth_service.check_access("")
    assert not result


def test_check_access_fail(app_container: ApplicationContainer):
    auth_service: AuthService = app_container.auth_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.get_auth_token.return_value = None
    result = auth_service.check_access("token")
    assert not result


def test_login(app_container: ApplicationContainer):
    jwt_token = "jwt_token"
    login_data = LoginData(username="user", password="login_password")
    user = User(
        id="user_id",
        username="user",
        password="password_hash",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user_by_username.return_value = user
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.check_password.return_value = True
    security_service_mock.create_auth_token.return_value = jwt_token
    auth_service: AuthService = app_container.auth_service()
    assert auth_service.login(login_data) == jwt_token
    user_repository_mock.get_user_by_username.assert_called_once_with(
        login_data.username
    )
    security_service_mock.check_password.assert_called_once_with(
        user.password, login_data.password
    )
    security_service_mock.create_auth_token.assert_called_once_with(user)


def test_login_when_user_not_exists(app_container: ApplicationContainer):
    login_data = LoginData(username="user", password="login_password")
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user_by_username.return_value = None
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    auth_service: AuthService = app_container.auth_service()
    with pytest.raises(WrongCredentialsException):
        auth_service.login(login_data)
    user_repository_mock.get_user_by_username.assert_called_once_with(
        login_data.username
    )
    security_service_mock.check_password.assert_not_called()
    security_service_mock.create_auth_token.assert_not_called()


def test_login_when_wrong_password(app_container: ApplicationContainer):
    jwt_token = "jwt_token"
    login_data = LoginData(username="user", password="login_password")
    user = User(
        id="user_id",
        username="user",
        password="password_hash",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user_by_username.return_value = user
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.check_password.return_value = False
    security_service_mock.create_auth_token.return_value = jwt_token
    auth_service: AuthService = app_container.auth_service()
    with pytest.raises(WrongCredentialsException):
        auth_service.login(login_data)
    user_repository_mock.get_user_by_username.assert_called_once_with(
        login_data.username
    )
    security_service_mock.check_password.assert_called_once_with(
        user.password, login_data.password
    )
    security_service_mock.create_auth_token.assert_not_called()


def test_who_am_i(app_container: ApplicationContainer):
    user_id = "user_id"
    user = User(
        id=user_id,
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    auth_service: AuthService = app_container.auth_service()
    assert auth_service.who_am_i(user_id) == user
    user_repository_mock.get_user.assert_called_once_with(user_id)
