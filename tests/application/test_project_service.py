import datetime
from unittest import mock

import pytest

from ryax.authorization.application.project_service import ProjectService
from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_exceptions import (
    ProjectAlreadyExistsException,
    ProjectNotFoundException,
)
from ryax.authorization.domain.project.project_factory import ProjectFactory
from ryax.authorization.domain.project.project_object import Project
from ryax.authorization.domain.project.project_repository import IProjectRepository
from ryax.authorization.domain.project.project_values import (
    AddUserToProjectData,
    ProjectCreateData,
    ProjectUpdateData,
)
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_repository import IUserRepository
from ryax.authorization.domain.user.user_values import (
    SetCurrentProjectData,
    UserRoles,
    UserStates,
)


def test_list_projects(app_container: ApplicationContainer):
    project_1 = Project(
        id="project_1", name="name", created_at=datetime.datetime.utcfromtimestamp(0)
    )
    project_2 = Project(
        id="project_2", name="name", created_at=datetime.datetime.utcfromtimestamp(0)
    )
    project_repository_mock: IProjectRepository = app_container.project_repository()
    project_repository_mock.list_projects = mock.MagicMock(
        return_value=[project_1, project_2]
    )
    project_service: ProjectService = app_container.project_service()
    assert project_service.list_projects() == [
        project_1,
        project_2,
    ]
    project_repository_mock.list_projects.assert_called_once()


def test_create_project(app_container: ApplicationContainer):
    create_project_data = ProjectCreateData(name="project name", id="id")
    project = Project(
        id="id", name="project_name", created_at=datetime.datetime.utcfromtimestamp(0)
    )
    project.check_all = mock.MagicMock()
    project_factory_mock: mock.MagicMock[
        ProjectFactory
    ] = app_container.project_factory()
    project_factory_mock.from_create_project = mock.MagicMock(return_value=project)
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project_by_name.return_value = None
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    assert project_service.create_project(create_project_data) == project.id
    project_factory_mock.from_create_project.assert_called_once_with(
        create_project_data
    )
    project_repository_mock.get_project_by_name.assert_called_once_with(
        create_project_data.name
    )
    project_repository_mock.add_project.assert_called_once_with(project)
    unit_of_work_mock.commit.assert_called_once()


def test_create_project_with_used_name(app_container: ApplicationContainer):
    create_project_data = ProjectCreateData(name="used name", id="id")
    project = Project(
        id="id", name="used name", created_at=datetime.datetime.utcfromtimestamp(0)
    )
    project.check_all = mock.MagicMock()
    project_factory_mock: mock.MagicMock[
        ProjectFactory
    ] = app_container.project_factory()
    project_factory_mock.from_create_project = mock.MagicMock()
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project_by_name.return_value = project
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    with pytest.raises(ProjectAlreadyExistsException):
        project_service.create_project(create_project_data)
    project_factory_mock.from_create_project.assert_not_called()
    project_repository_mock.get_project_by_name.assert_called_once_with(
        create_project_data.name
    )
    project_repository_mock.add_project.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()


def test_get_project(app_container: ApplicationContainer):
    project_id = "project_id"
    project = Project(
        id=project_id, name="name", created_at=datetime.datetime.utcfromtimestamp(0)
    )
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(return_value=project)
    project_service: ProjectService = app_container.project_service()
    assert project_service.get_project(project_id) == project
    project_repository_mock.get_project.assert_called_with(project_id)


def test_get_project_when_not_exists(app_container: ApplicationContainer):
    project_id = "project_id"
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(
        side_effect=ProjectNotFoundException
    )
    project_service: ProjectService = app_container.project_service()
    with pytest.raises(ProjectNotFoundException):
        project_service.get_project(project_id)
    project_repository_mock.get_project.assert_called_once_with(project_id)


def test_delete_project(app_container: ApplicationContainer):
    project_id = "project_id"
    project = Project(
        id="project_id",
        name="Project_name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )

    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(return_value=project)
    project.remove_all_users = mock.MagicMock()

    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()

    project_service.delete_project(project_id)
    project_repository_mock.get_project.assert_called_once_with(project_id)
    project.remove_all_users.assert_called_once()
    project_repository_mock.delete_project.assert_called_once_with(project)
    project_unit_mock.commit.assert_called()


def test_delete_project_when_not_exists(app_container: ApplicationContainer):
    project_id = "project_id"
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(
        side_effect=ProjectNotFoundException
    )
    project_repository_mock.delete_project = mock.MagicMock()
    project_service: ProjectService = app_container.project_service()
    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    with pytest.raises(ProjectNotFoundException):
        project_service.delete_project(project_id)
    project_repository_mock.get_project.assert_called_once_with(project_id)
    project_repository_mock.delete_project.assert_not_called()
    project_unit_mock.commit.assert_not_called()


def test_update_project(app_container: ApplicationContainer):
    project_id = "project_id"
    project = Project(
        id="project_id",
        name="Project_name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project.update_data = mock.MagicMock()
    update_data = ProjectUpdateData(name="new_project_name")

    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(return_value=project)

    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    assert project_service.update_project(project_id, update_data) == project
    project.update_data.assert_called_once_with(update_data)
    project_repository_mock.get_project.assert_called_with(project_id)
    project_unit_mock.commit.assert_called_once()


def test_update_project_when_not_found(app_container: ApplicationContainer):
    project_id = "project_id"
    update_data = ProjectUpdateData(name="new_project_name")

    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(
        side_effect=ProjectNotFoundException
    )

    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    with pytest.raises(ProjectNotFoundException):
        project_service.update_project(project_id, update_data)
        project_repository_mock.get_project.assert_called_once_with(project_id)
        project_unit_mock.commit.assert_not_called()


def test_add_user_to_project(app_container: ApplicationContainer):
    project_id = "project_id"
    user_id = "user_id"
    project = Project(
        id=project_id,
        name="Project_name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    user = User(
        id=user_id,
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    project.add_user_to_project = mock.MagicMock()
    add_user_data = AddUserToProjectData(user_id=user_id)
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    user_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.user_repository()
    project_repository_mock.get_project = mock.MagicMock(return_value=project)
    user_repository_mock.get_user = mock.MagicMock(return_value=user)
    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    project_service.add_user_to_project(project_id, add_user_data)
    project_repository_mock.get_project.assert_called_once_with(project_id)
    user_repository_mock.get_user.assert_called_once_with(add_user_data.user_id)
    project.add_user_to_project.assert_called_once_with(user)
    project_unit_mock.commit.assert_called_once()


def test_delete_user_from_project(app_container: ApplicationContainer):
    project_id = "project_id"
    user_id = "user_id"
    project = Project(
        id=project_id,
        name="Project_name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    project.delete_user_from_project = mock.MagicMock()
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    project_repository_mock.get_project = mock.MagicMock(return_value=project)
    project_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service: ProjectService = app_container.project_service()
    assert project_service.delete_user_from_project(project_id, user_id) == user_id
    project_repository_mock.get_project.assert_called_once_with(project_id)
    project.delete_user_from_project.assert_called_once_with(user_id)
    project_unit_mock.commit.assert_called_once()


def test_get_user_projects(app_container: ApplicationContainer):
    user_id = "user_id"
    user = User(
        id=user_id,
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    projects = [
        Project(
            id="project_1",
            name="name",
            created_at=datetime.datetime.utcfromtimestamp(0),
        ),
        Project(
            id="project_2",
            name="name",
            created_at=datetime.datetime.utcfromtimestamp(0),
        ),
    ]
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    user_repository_mock.get_user_projects.return_value = projects
    project_service: ProjectService = app_container.project_service()
    assert project_service.get_user_projects(user_id) == projects
    user_repository_mock.get_user.assert_called_once_with(user_id)
    user_repository_mock.get_user_projects.assert_called_once_with(user)


def test_get_current_project(app_container: ApplicationContainer):
    user_id = "user_id"
    user = User(
        id=user_id,
        current_project=Project(
            id="project_1",
            name="name",
            created_at=datetime.datetime.utcfromtimestamp(0),
        ),
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    project_service: ProjectService = app_container.project_service()
    assert project_service.get_current_project(user_id) == user.current_project
    user_repository_mock.get_user.assert_called_once_with(user_id)


def test_set_current_project(app_container: ApplicationContainer):
    user_id = "user_id"
    project_id = "project_id"
    current_project_data = SetCurrentProjectData(project_id=project_id)
    user = User(
        id=user_id,
        password="pass",
        username="username",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    project = Project(
        id=project_id,
        name="Project_name",
        created_at=datetime.datetime.utcfromtimestamp(0),
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    project_repository_mock: mock.MagicMock[
        IProjectRepository
    ] = app_container.project_repository()
    user_repository_mock.get_user.return_value = user
    project_repository_mock.get_project.return_value = project
    user.set_current_project = mock.MagicMock()
    project_service: ProjectService = app_container.project_service()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    project_service.set_current_project(user_id, current_project_data)
    user_repository_mock.get_user.assert_called_once_with(user_id)
    project_repository_mock.get_project.assert_called_once_with(project_id)
    user.set_current_project.assert_called_once_with(project)
    unit_of_work_mock.commit.assert_called_once()
