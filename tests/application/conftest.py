from unittest import mock

import pytest

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.project.project_repository import IProjectRepository
from ryax.authorization.domain.services.security_service import ISecurityService
from ryax.authorization.domain.user.user_factory import UserFactory
from ryax.authorization.domain.user.user_repository import IUserRepository


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()

    security_service = mock.MagicMock(ISecurityService)
    app_container.security_service.override(security_service)

    project_repository = mock.MagicMock(IProjectRepository)
    app_container.project_repository.override(project_repository)

    user_repository = mock.MagicMock(IUserRepository)
    app_container.user_repository.override(user_repository)

    user_factory_mock = mock.MagicMock(UserFactory)
    app_container.user_factory.override(user_factory_mock)

    unit_of_work: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    unit_of_work.projects = project_repository
    unit_of_work.users = user_repository
    app_container.unit_of_work.override(unit_of_work)

    return app_container
