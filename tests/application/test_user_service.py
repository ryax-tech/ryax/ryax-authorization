import datetime
from unittest import mock

from ryax.authorization.application.unit_of_work import IUnitOfWork
from ryax.authorization.application.user_service import UserService
from ryax.authorization.container import ApplicationContainer
from ryax.authorization.domain.services.security_service import ISecurityService
from ryax.authorization.domain.user.user_object import User
from ryax.authorization.domain.user.user_repository import IUserRepository
from ryax.authorization.domain.user.user_values import (
    UserRoles,
    UserStates,
    UserUpdateData,
)


def test_list_users(app_container: ApplicationContainer):
    user_1 = User(
        id="user1",
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_2 = User(
        id="user2",
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.list_users.return_value = [user_1, user_2]
    user_service: UserService = app_container.user_service()
    assert user_service.list_users() == [user_1, user_2]
    user_repository_mock.list_users.assert_called_once()


def test_get_user(app_container: ApplicationContainer):
    user_id = "user_id"
    user = User(
        id=user_id,
        username="username",
        password="password",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    user_service: UserService = app_container.user_service()
    assert user_service.get_user(user_id) == user
    user_repository_mock.get_user.assert_called_once_with(user_id)


def test_update_user(app_container: ApplicationContainer):
    user_id = "user_id"
    user_data = UserUpdateData(
        username="username",
        password="you shall not pass",
        role=None,
        email=None,
        comment=None,
    )
    user = User(
        id=user_id,
        password="pass",
        username="username",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    created_password = "created_password_hash"
    updated_user = User(
        id=user_id,
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        state=UserStates.ok,
        **user_data.__dict__
    )
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.security_service()
    security_service_mock.create_user_password.return_value = created_password
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    user.update_data = mock.MagicMock(return_value=updated_user)
    user_service: UserService = app_container.user_service()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    assert user_service.update_user(user_id, user_data) == user
    security_service_mock.create_user_password.assert_called_once_with(
        user_data.password
    )
    user_repository_mock.get_user.assert_called_with(user_id)
    user.update_data.assert_called_once_with(user_data, created_password)
    unit_of_work_mock.commit.assert_called_once()


def test_delete_user(app_container: ApplicationContainer):
    user_id = "user_id"
    user = User(
        id=user_id,
        password="pass",
        username="username",
        created_at=datetime.datetime.utcfromtimestamp(0),
        modified_at=datetime.datetime.utcfromtimestamp(0),
        role=UserRoles.user,
        state=UserStates.ok,
        email="email",
        comment="",
    )
    user_repository_mock: mock.MagicMock[
        IUserRepository
    ] = app_container.user_repository()
    user_repository_mock.get_user.return_value = user
    user_repository_mock.delete_user.return_value = None
    user_service: UserService = app_container.user_service()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    user_service.delete_user(user_id)
    user_repository_mock.get_user.assert_called_once_with(user_id)
    user_repository_mock.delete_user.assert_called_once_with(user)
    unit_of_work_mock.commit.assert_called_once()
